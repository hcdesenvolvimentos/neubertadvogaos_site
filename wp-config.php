<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_neubertadvogados_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '123' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wu=;+s~5;,vA4jaTIg=no!7=zM<^plT~}8o=r2q]DI{PeW-hd>Bycc>nCzaNP%Gn' );
define( 'SECURE_AUTH_KEY',  'St1kBd[iL e-Zl{oqOVwX@twx}a<1bT,!G+|EdtZw^/4|r?Dh{u8Zl(;!MDp,>3t' );
define( 'LOGGED_IN_KEY',    '8O d_ ~},Hzu_e~97dY)oEwTCQ<$k@7no(*/q.gK[m/_$[_Nk0Mmh~fcr93Mgir)' );
define( 'NONCE_KEY',        '_AYko|^k@[o#vqo`xE K34[v: 0<8?UI#DH_LpW8Z9`K7h?]`9ZE7NWB|&TRj^qD' );
define( 'AUTH_SALT',        ';b^,STj{1u{]|Kx.,d^z*_V9n]H?kE~QH*v_x}Na7:MkGH_j&<5ltjZr+kH#>*8H' );
define( 'SECURE_AUTH_SALT', '-M{UuEq=QMobkQ@YCDR5Ze&Y,YMbK+VC)^8$?#E@Bg;9tLA~$oaUv?%9>o=jvC [' );
define( 'LOGGED_IN_SALT',   '2 8E6BKqc)F8Y?T3hDpriFS;)B02i8Q2FR6EoCA2Lvp[2Om~z[B vjzjl~4c4Z64' );
define( 'NONCE_SALT',       'Mbue^%iGH8Jg*pPd>KGH%s?4D!Ux.+=E&2!FzLI5fiH`8l)1%ZQp9w[q,#9r bHo' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'na_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
