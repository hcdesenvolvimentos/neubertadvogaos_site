<?php

/**
 * Plugin Name: Base Neubert Advogados
 * Description: Controle base do tema Neubert Advogados.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseNubertAdvogados () {

		// TIPOS DE CONTEÚDO
		conteudosNubertAdvogados();

		taxonomiaNubertAdvogados();

		metaboxesNubertAdvogados();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosNubertAdvogados (){

		// TIPOS DE CONTEÚDO
		tipoDestaque();

		// TIPOS DE CONTEÚDO
		tipoServico();

		// TIPO PROFISSIONAIS
		tipoProfissional();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail', 'editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}


	// CUSTOM POST TYPE SERVIÇOS
	function tipoServico() {

		$rotulosServico = array(
								'name'               => 'serviços',
								'singular_name'      => 'serviço',
								'menu_name'          => 'Serviços',
								'name_admin_bar'     => 'serviços',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo serviço',
								'new_item'           => 'Novo serviço',
								'edit_item'          => 'Editar serviço',
								'view_item'          => 'Ver serviço',
								'all_items'          => 'Todos os serviços',
								'search_items'       => 'Buscar serviço',
								'parent_item_colon'  => 'Dos serviços',
								'not_found'          => 'Nenhum serviço cadastrado.',
								'not_found_in_trash' => 'Nenhum serviço na lixeira.'
							);

		$argsServico 	= array(
								'labels'             => $rotulosServico,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'servicos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail', 'editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servico', $argsServico);

	}

	// CUSTOM POST TYPE PROFISSIONAIS
	function tipoProfissional() {

		$rotulosProfissional = array(
								'name'               => 'Profissionais',
								'singular_name'      => 'profissional',
								'menu_name'          => 'Profissionais',
								'name_admin_bar'     => 'Profissionais',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo profissional',
								'new_item'           => 'Novo profissional',
								'edit_item'          => 'Editar profissional',
								'view_item'          => 'Ver profissional',
								'all_items'          => 'Todos os profissionais',
								'search_items'       => 'Buscar profissional',
								'parent_item_colon'  => 'Dos profissionais',
								'not_found'          => 'Nenhum profissional cadastrado.',
								'not_found_in_trash' => 'Nenhum profissional na lixeira.'
							);

		$argsProfissionais 	= array(
								'labels'             => $rotulosProfissional,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-id',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'profissional' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('profissional', $argsProfissionais);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaNubertAdvogados () {		
		taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesNubertAdvogados(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'NubertAdvogados_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Link do destaque: ',
						'id'    => "{$prefix}destaque_link",
						'desc'  => 'Link destaque',
						'type'  => 'text'
					),	

					array(
						'name'  => 'Check se texto for pra direita: ',
						'id'    => "{$prefix}destaque_checkbox",
						'desc'  => 'Marque se o texto for à direita da imagem.',
						'type'  => 'checkbox'
					),	
									
				),
			);

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxServicos',
				'title'			=> 'Detalhes do serviço',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					
					array(
						'name'  => 'Link do botão: ',
						'id'    => "{$prefix}servico_link_botao",
						'desc'  => 'Link do botão "Agende sua orientação"',
						'type'  => 'text'
					),	
									
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesNubertAdvogados(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerNubertAdvogados(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseNubertAdvogados');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseNubertAdvogados();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );