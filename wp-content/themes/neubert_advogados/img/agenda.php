<?php



/**

 * Template Name: Agenda

 * Description: Página Agenda

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package neubert_advogados

 */



get_header();

?>



<div class="pg pg-agenda">

	<div class="menuContato">

		<nav>

			<ul>

				<li class="whatsapp">

					<a href="#">

						<i class="fab fa-whatsapp"></i>

						<p>whatsapp</p>

					</a>

				</li>

				<li>

					<a href="#">

						<i class="far fa-comments"></i>

						<p>chat</p>

					</a>

				</li>

				<li>

					<a href="#">

						<i class="far fa-user-circle"></i>

						<p>login</p>

					</a>

				</li>

			</ul>

		</nav>

	</div>



	<section class="sessaoInicialAgenda">

		<h3><?php echo $configuracao['opt_titulo_pagina_agenda'] ?></h3>



		<article class="paddingCorrecao">

			<p><?php echo $configuracao['opt_desc_pagina_agenda'] ?></p>

		</article>

	</section>



	<section class="sessaoEscolhaAtendimento">

		<h3><?php echo $configuracao['opt_titulo_seu_atendimento'] ?></h3>

		<ul>

			<li>

				<a href="#agendamento" class="scrollTop">

					<figure>

						<img src="<?php echo $configuracao['opt_imagem_juriflex']['url'] ?>" alt="Imagem <?php echo $configuracao['opt_titulo_juriflex'] ?>">

					</figure>

					<h2><strong><?php echo $configuracao['opt_titulo_juriflex'] ?></strong></h2>

				</a>

			</li>

			<li>

				<a href="">

					<figure>

						<img src="<?php echo $configuracao['opt_imagem_orientacao_avulsa']['url'] ?>" alt="Imagem <?php echo $configuracao['opt_titulo_orientacao_avulsa'] ?>">

					</figure>

					<h2><?php echo $configuracao['opt_titulo_orientacao_avulsa'] ?></h2>

				</a>

			</li>

		</ul>

	</section>



	<section class="sessaoAgendaJurifelx" id="agendamento">

		<h3><?php echo $configuracao['opt_titulo_agendamento_juriflex'] ?></h3>

		<p class="paddingCorrecao"><?php echo $configuracao['opt_desc_agendamento_juriflex'] ?></p>



		<ul class="instrucao">

			<li><?php echo $configuracao['opt_tempo_agendamento'] ?></li>

			<li><?php echo $configuracao['opt_agendamento_consulta_gratuita'] ?></li>

			<li><?php echo $configuracao['opt_agendamento_vamos_ate_voce'] ?></li>

		</ul>



		<div class="areaAgendamento" >

			<div class="row">

				<div class="col-sm-8">

					<div class="carrossel-calendario">

						<div class="carrosselCalendario" id="carrosselCalendario">

							<div class="item">

								<h5>Janeiro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="04/05">03</span>

										<span data-dia="05/05">10</span>

										<span data-dia="06/05">17</span>

										<span data-dia="07/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Fevereiro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="04/05">03</span>

										<span data-dia="05/05">10</span>

										<span data-dia="06/05">17</span>

										<span data-dia="07/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Março 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="04/05">03</span>

										<span data-dia="05/05">10</span>

										<span data-dia="06/05">17</span>

										<span data-dia="07/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Abril 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="04/05">03</span>

										<span data-dia="05/05">10</span>

										<span data-dia="06/05">17</span>

										<span data-dia="07/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>

							<div class="item">

								<h5>Maio 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="04/05">03</span>

										<span data-dia="05/05">10</span>

										<span data-dia="06/05">17</span>

										<span data-dia="07/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>

							<div class="item">

								<h5>Junho 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>

							<div class="item">

								<h5>Julho 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Agosto 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Setembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Outubro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Novembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Dezembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sex</p>

										<span>27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

									<li>

										<p>Sáb</p>

										<span data-dia="03/05">27</span>

										<span data-dia="03/05">03</span>

										<span data-dia="03/05">10</span>

										<span data-dia="03/05">17</span>

										<span data-dia="03/05">24</span>

									</li>

								</ul>

							</div>

						</div>



						<!-- BOTÕES CARROSSEL CALENDÁRIO -->

						<div class="btnCarrossel">

							<button id="flechaEsquerda" class="esquerdacarrosselCalendario"><img src="<?php echo get_template_directory_uri() ?>/img/left.svg" alt="Flexas"></button>

							<button id="flechaDireita" class="direitacarrosselCalendario"><img src="<?php echo get_template_directory_uri() ?>/img/right.svg" alt="Flexas"></button>

						</div>

					</div>

				</div>	

				<div class="col-sm-4">

					<div class="areaFormulario">

						<img src="<?php echo get_template_directory_uri(); ?>/img/calendar.png" alt="calendar">

						<p>Selecione seu <br> Agendamento:</p>

						<?php echo do_shortcode('[contact-form-7 id="51" title="Formulário de agendamento"]'); ?>

					</div>

				</div>	

			</div>

		</div>

	</section>



	<!-- FORMULÁRIO DE CONTATO -->

	<?php include (TEMPLATEPATH . '/templates/formularioContato.php');  ?>

</div>



<?php get_footer();