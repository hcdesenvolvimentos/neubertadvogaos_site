<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package neubert_advogados
 */
global $configuracao;
?>

		<footer class="rodape">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<figure>
							<img src="<?php echo $configuracao['opt_logo_rodape']['url'] ?>" alt="<?php bloginfo( 'name' ); ?>">
							<figcaption class="hidden"><?php bloginfo( 'name' ); ?></figcaption>
						</figure>
					</div>
					<div class="col-sm-6">
						<div class="redesSociais_contato">
							<a href="tel:<?php echo $configuracao['opt_contato'] ?>"><span><?php echo $configuracao['opt_contato'] ?></span></a>
							<p><?php echo $configuracao['opt_endereco']?></p>
							<a href="<?php echo $configuracao['opt_youtube']?>" target="_blank"><b><i class="fab fa-linkedin-in"></i></b></a>
							<a href="<?php echo $configuracao['opt_instagram']?>" class="instagram" target="_blank"><b><i class="fab fa-instagram"></i></b></a>
							<a href="<?php echo $configuracao['opt_facebook']?>" target="_blank"><b><i class="fab fa-facebook-f"></i></b></a>
						</div>
					</div>
				</div>
			</div>

			<div class="copyright">
				<p><?php echo $configuracao['rodape_titulo_texto']?></p>
			</div>
		
		</footer>
	
<?php wp_footer(); ?>
<!-- begin olark code -->
<script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('1635-449-10-8633');</script>
<!-- end olark code -->
</body>
</html>
