<section class="sessaoContato" id="contato">
	<h4><?php echo $configuracao['config_area_contato_titulo']; ?></h4>
	<p><?php echo $configuracao['config_area_contato_texto']; ?></p>
	<span><?php echo $configuracao['config_area_contato_desc']; ?></span>

	<div class="container">
		<div class="areaForm">
			<div class="row">
				<?php echo do_shortcode('[contact-form-7 id="32" title="Formulário de contato"]'); ?>
			</div>
		</div>
	</div>
</section>