  
<?php
  //SESSÃO QUEM SOMOS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página quem somos', 'redux-framework-demo' ),
            'id'               => 'configuracoes_quem_somos',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Seção principal', 'redux-framework-demo' ),
                    'id'               => 'config_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_desc_quem_somos',
                            'type'     => 'editor',
                            'title'    => __( 'Descrição quem somos', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Seção atributos', 'redux-framework-demo' ),
                    'id'               => 'config_quem_somos_atributos',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_imagem_missao',
                            'type'     => 'media',
                            'title'    => __( 'Imagem missão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_imagem_visao',
                            'type'     => 'media',
                            'title'    => __( 'Imagem visão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_imagem_valores',
                            'type'     => 'media',
                            'title'    => __( 'Imagem valores', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_missao',
                            'type'     => 'text',
                            'title'    => __( 'Título missão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_visao',
                            'type'     => 'text',
                            'title'    => __( 'Título visão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_valores',
                            'type'     => 'text',
                            'title'    => __( 'Título valores', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_desc_missao',
                            'type'     => 'editor',
                            'title'    => __( 'Descrição missão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_desc_visao',
                            'type'     => 'editor',
                            'title'    => __( 'Descrição visão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_desc_valores',
                            'type'     => 'editor',
                            'title'    => __( 'Descrição valores', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Seção profissionais', 'redux-framework-demo' ),
                    'id'               => 'config_quem_somos_profissionais',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_profissionais',
                            'type'     => 'text',
                            'title'    => __( 'Título profissionais', 'redux-framework-demo' ),
                            'desc'    => __( '"Profissionais"', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            
