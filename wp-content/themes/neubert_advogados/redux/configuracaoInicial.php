
<?php
        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página inicial', 'redux-framework-demo' ),
            'id'               => 'configuracoes_pg_inicial',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
          
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área Previdenciário', 'redux-framework-demo' ),
                    'id'               => 'config_area_previsenciario',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
				       array(
                            'id'       => 'config_area_previsenciario_imagem',
                            'type'     => 'media',
                            'title'    => __( 'Imagem Ilustrativa', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
				        array(
				            'id'       => 'config_area_previsenciario_titulo',
				            'type'     => 'text',
				            'title'    => __( 'Título', 'redux-framework-demo' ),
				            'desc'    => __( '', 'redux-framework-demo' ),
				        ),

                        array(
                            'id'       => 'config_area_previsenciario_textto',
                            'type'     => 'editor',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),


                        array(
                            'id'       => 'config_area_previsenciario_icone_1',
                            'type'     => 'media',
                            'title'    => __( 'Primeiro ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_1',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_icone_2',
                            'type'     => 'media',
                            'title'    => __( 'Segundo ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_2',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_icone_3',
                            'type'     => 'media',
                            'title'    => __( 'Terceiro ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_3',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_icone_4',
                            'type'     => 'media',
                            'title'    => __( 'Quarto ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_4',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_pedido',
                            'type'     => 'text',
                            'title'    => __( 'Área Realize seu pedido', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_pedido_desc',
                            'type'     => 'text',
                            'title'    => __( 'Área Realize seu pedido descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_previsenciario_texto_pedido_link',
                            'type'     => 'text',
                            'title'    => __( 'Link botão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );

            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área Vantagens', 'redux-framework-demo' ),
                    'id'               => 'config_area_vantagens',
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'config_area_vantagens_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_icone_1',
                            'type'     => 'media',
                            'title'    => __( 'Primeiro ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                         array(
                            'id'       => 'config_area_vantagens_titulo_1',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_texto_1',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_icone_2',
                            'type'     => 'media',
                            'title'    => __( 'Segundo ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_titulo_2',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_texto_2',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_icone_3',
                            'type'     => 'media',
                            'title'    => __( 'Terceiro ícone', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_titulo_3',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_vantagens_texto_3',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                    )
                )
            );


             Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área Juriflex', 'redux-framework-demo' ),
                    'id'               => 'config_area_Juriflex',
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         array(
                            'id'       => 'config_area_Juriflex_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título da área', 'redux-framework-demo' ),
                            'desc'    => __( 'Contrate a Juriflex Acessoria Jurídica Empresarial', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'config_area_Juriflex_texto',
                            'type'     => 'editor',
                            'title'    => __( 'Contrate a Juriflex Acessoria Jurídica Empresarial', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_Juriflex_video',
                            'type'     => 'text',
                            'title'    => __( 'ID Vídeo ', 'redux-framework-demo' ),
                            'desc'    => __( 'https://www.youtube.com/watch?time_continue=1&v=  <strong>ImAYDYannNY</strong>', 'redux-framework-demo' ),
                        ),

                         array(
                            'id'       => 'config_area_Juriflex_texto_pedido',
                            'type'     => 'text',
                            'title'    => __( 'Área Realize seu pedido', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_Juriflex_texto_pedido_desc',
                            'type'     => 'text',
                            'title'    => __( 'Área Realize seu pedido descrição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_Juriflex_texto_pedido_link',
                            'type'     => 'text',
                            'title'    => __( 'Link botão', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                    )
                )
            );

            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área Empresarial', 'redux-framework-demo' ),
                    'id'               => 'config_area_Empresarial',
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         array(
                            'id'       => 'config_area_Empresarial_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título da área', 'redux-framework-demo' ),
                            'desc'    => __( 'Empresarial', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_Empresarial_texto',
                            'type'     => 'editor',
                            'title'    => __( 'Texto', 'redux-framework-demo' ),
                            
                        ),

                         array(
                            'id'       => 'config_area_Empresarial_link',
                            'type'     => 'text',
                            'title'    => __( 'Link botão', 'redux-framework-demo' ),
                            
                        ),
                      

                    )
                )
            );

             Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área contato', 'redux-framework-demo' ),
                    'id'               => 'config_area_contato',
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         array(
                            'id'       => 'config_area_contato_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título da área', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                        array(
                            'id'       => 'config_area_contato_texto',
                            'type'     => 'text',
                            'title'    => __( 'SubTitulo', 'redux-framework-demo' ),
                            
                        ), 

                        array(
                            'id'       => 'config_area_contato_desc',
                            'type'     => 'text',
                            'title'    => __( 'Descrição', 'redux-framework-demo' ),
                            
                        ),

                      

                    )
                )
            );