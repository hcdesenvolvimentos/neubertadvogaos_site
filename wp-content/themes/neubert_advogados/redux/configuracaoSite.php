  
<?php
  //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações do site', 'redux-framework-demo' ),
            'id'               => 'configuracoes_site',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações site', 'redux-framework-demo' ),
                    'id'               => 'config_site',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_logo',
                            'type'     => 'media',
                            'title'    => __( 'Logo', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),

                          array(
                            'id'       => 'opt_logo_rodape',
                            'type'     => 'media',
                            'title'    => __( 'Logo rodapé', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'rodape_titulo_texto',
                            'type'     => 'text',
                            'title'    => __( 'Copyright', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                      
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações redes sociais', 'redux-framework-demo' ),
                    'id'               => 'config_redesSociais',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_facebook',
                            'type'     => 'text',
                            'title'    => __( 'facebook', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'opt_youtube',
                            'type'     => 'text',
                            'title'    => __( 'youtube', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_instagram',
                            'type'     => 'text',
                            'title'    => __( 'instagram', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                       
                    )
                )
            );

             // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Configurações endereço', 'redux-framework-demo' ),
                    'id'               => 'config_endereco',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_endereco',
                            'type'     => 'text',
                            'title'    => __( 'Endereço', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                         array(
                            'id'       => 'opt_contato',
                            'type'     => 'text',
                            'title'    => __( 'Contato', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
