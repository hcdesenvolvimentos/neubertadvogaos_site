  
<?php
  //SESSÃO AGENDA
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página agenda', 'redux-framework-demo' ),
            'id'               => 'configuracoes_agenda',
            'desc'             => __( '', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            ) 
        );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Seção agenda', 'redux-framework-demo' ),
                    'id'               => 'config_agenda',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_pagina_agenda',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                            'desc'    => __( '"FAÇA SEU AGENDAMENTO!"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_desc_pagina_agenda',
                            'type'     => 'editor',
                            'title'    => __( 'Descição', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_seu_atendimento',
                            'type'     => 'text',
                            'title'    => __( 'Título seu atendimento', 'redux-framework-demo' ),
                            'desc'    => __( '"Escolha seu atendimento"', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_imagem_juriflex',
                            'type'     => 'media',
                            'title'    => __( 'Imagem Juriflex', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_imagem_orientacao_avulsa',
                            'type'     => 'media',
                            'title'    => __( 'Imagem Orientação Avulsa', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_juriflex',
                            'type'     => 'text',
                            'title'    => __( 'Título Juriflex', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_titulo_orientacao_avulsa',
                            'type'     => 'text',
                            'title'    => __( 'Título Orientação Avulsa', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            // CONFIGURAÇÕES DO SITE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Seção agendamento Juriflex', 'redux-framework-demo' ),
                    'id'               => 'config_agendamento_juriflex',
                    'subsection'       => true,
                    'desc'             => __( 'Configurações', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_titulo_agendamento_juriflex',
                            'type'     => 'text',
                            'title'    => __( 'Título Agendamento Juriflex', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_desc_agendamento_juriflex',
                            'type'     => 'editor',
                            'title'    => __( 'Descriçao Agendamento Juriflex', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_desc_agendamento_juriflex',
                            'type'     => 'editor',
                            'title'    => __( 'Descriçao Agendamento Juriflex', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_tempo_agendamento',
                            'type'     => 'text',
                            'title'    => __( 'Vantagens', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_agendamento_consulta_gratuita',
                            'type'     => 'text',
                            'title'    => __( 'Vantagens', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_agendamento_vamos_ate_voce',
                            'type'     => 'text',
                            'title'    => __( 'Vantagens', 'redux-framework-demo' ),
                            'desc'    => __( '', 'redux-framework-demo' ),
                        ),
                    )
                )
            );
            
