$(function(){



	/*****************************************

		SCRIPTS PÁGINA INICIAL

	*******************************************/

	

	//CARROSSEL DE DESTAQUE

	$("#carrosselDestaque").owlCarousel({
		items : 1,
		dots: true,
		loop: false,
		lazyLoad: true,
		mouseDrag:true,
		touchDrag  : false,
		autoplay:true,
		autoplayTimeout:10000,
		autoplayHoverPause:true,
		smartSpeed: 450,	
		animateOut: 'fadeOut',
	});

	var carrosselDestaque = $("#carrosselDestaque").data('owlCarousel');

	$('.esquerdaCarrossel').click(function(){ carrosselDestaque.prev(); });

	$('.direitaCarrossel').click(function(){ carrosselDestaque.next(); });



	//CARROSSEL DE DESTAQUE

	$("#carrosselServico").owlCarousel({
		items : 4,
		dots: true,
		loop: false,
		lazyLoad: true,
		mouseDrag:true,
		touchDrag: false,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		smartSpeed: 450,	
		animateOut: 'fadeOut',
		responsiveClass:true,			    
		responsive:{
			320:{
				items:1
			},
			540:{
				items:2
			},
			768:{
				items:3
			},
			991:{
				items:3
			},
			1024:{
				items:4
			},
		}

	});

	$(".pg-agenda .carrosselCalendario .item ul li span").click(function(e){
		$(".pg-agenda .carrosselCalendario .item ul li span").removeClass('ativo');
		$(this).addClass('ativo');
		$('#data-atendimento-juriflex').attr('value', $(this).attr('data-dia'));
		$('#data-atendimento-orientacao-avulsa').attr('value', $(this).attr('data-dia'));

		console.log($(this).attr('data-dia'));
	});

	$('a.scrollTop').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}

		}
	});

	$('.scrollTopLink a').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}

	});

	$('header.topo .btnAbrirMenuMobile').click(function(e){
		$('header.topo .menuMobile').addClass('abrirMenuMobile');
	});

	$('header.topo .btnFecharMenuMobile').click(function(e){
		$('header.topo .menuMobile').removeClass('abrirMenuMobile');
	});

	$('header.topo nav ul li a').click(function(e){
		$('header.topo .menuMobile').removeClass('abrirMenuMobile');
	});

	$(document).scroll(function () {
		let alturaScroll =  $(document).scrollTop();
		if(alturaScroll > 150){
			$('header.topo .menuPrincipal').addClass('menuThin');
			$('header.topo .logoMarca figure img').attr('src', 'http://localhost/projetos/neubertadvogaos_site/wp-content/themes/neubert_advogados/img/logoMarcaMenor.png')
		}else{
			$('header.topo .menuPrincipal').removeClass('menuThin');
			$('header.topo .logoMarca figure img').attr('src', 'http://localhost/projetos/neubertadvogaos_site/wp-content/themes/neubert_advogados/img/logoMarca.png')
		}
	});

	let isClicked = false;
	$('.pg-agenda .sessaoEscolhaAtendimento ul li a').click(function(e){
		e.preventDefault();
		isClicked = true;

		let dataId = $(this).attr('data-id');

		$('.pg-agenda .sessaoEscolhaAtendimento ul li a').removeClass('hover-h2');
		$(this).addClass('hover-h2');

		$('.pg-agenda .sessaoEscolhaAtendimento ul li').removeClass('after-li');
		$(this).parent('li').addClass('after-li');
		
		$('.pg-agenda .sessaoAgendaJurifelx').css('display', 'none');
		$('.pg-agenda .sessaoAgendaJurifelx#'+dataId).slideDown();

		//CARROSSEL CALENDÁRIO
		$('#carrosselCalendario'+dataId).owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : false,
			smartSpeed: 450,	
			animateOut: 'fadeOut',
		});
		let carrosselCalendario = $('#carrosselCalendario'+dataId).data('owlCarousel');
		$('.esquerdacarrosselCalendario').click(function(){ carrosselCalendario.prev(); });
		$('.direitacarrosselCalendario').click(function(){ carrosselCalendario.next(); });

		$('#input-servico-agenda-juriflex').attr('value', 'Juriflex');
		$('#input-servico-agenda-orientacao-avulsa').attr('value', 'Orientação avulsa');
	});

	$('.pg-inicial .sessaoServicos .areaServicos .item').click(function(e){
		let dataId = $(this).attr('data-id');
		$('.pg-inicial .sessaoServicos .areaServicos .item').removeClass('item-ativo');
		$('.pg-inicial .single-servico').removeClass('sessao-ativa');

		$(this).addClass('item-ativo');
		$('.pg-inicial .single-servico#'+dataId).addClass('sessao-ativa');
	});

	$('.pg .menuContato nav ul li a').click(function(e){
		e.preventDefault();
		// $('.olark-launch-button').css('visibility', 'initial');
		$('.olark-launch-button').click();
	});

});