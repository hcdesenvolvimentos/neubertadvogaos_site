<?php



/**

 * Template Name: Agenda

 * Description: Página Agenda

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package neubert_advogados

 */



get_header();

?>



<div class="pg pg-agenda">
	<div class="facaSeuAgendamento">
		<div class="menuContato">

			<nav>

				<ul>
					<li class="whatsapp">
						<a href="https://api.whatsapp.com/send?phone=<?php echo $configuracao['opt_whatsapp'] ?>&text=Ol%C3%A1!" target="_blank">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/whatsapp.png" alt="">
								<figcaption>whatsapp</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="<?php echo "#" ?>" class="chat">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/chat.png" alt="">
								<figcaption>chat</figcaption>
							</figure>
						</a>
					</li>
					<li>
						<a href="<?php echo $configuracao['opt_facebook'] ?>">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/user.png" alt="">
								<figcaption>login</figcaption>
							</figure>
						</a>
					</li>
				</ul>

			</nav>

		</div>



		<section class="sessaoInicialAgenda">

			<h3><?php echo $configuracao['opt_titulo_pagina_agenda'] ?></h3>



			<article class="paddingCorrecao">

				<p><?php echo $configuracao['opt_desc_pagina_agenda'] ?></p>

			</article>

		</section>



		<section class="sessaoEscolhaAtendimento">
			<h3><?php echo $configuracao['opt_titulo_seu_atendimento'] ?></h3>
			<ul>
				<li>
					<a href="#0" class="scrollTop" data-id="0">
						<figure>
							<img src="<?php echo $configuracao['opt_imagem_juriflex']['url'] ?>" alt="Imagem <?php echo $configuracao['opt_titulo_juriflex'] ?>">
						</figure>
						<h2><?php echo $configuracao['opt_titulo_juriflex'] ?></h2>
					</a>
				</li>
				<li>
					<a href="#1" class="scrollTop" data-id="1">
						<figure>
							<img src="<?php echo $configuracao['opt_imagem_orientacao_avulsa']['url'] ?>" alt="Imagem <?php echo $configuracao['opt_titulo_orientacao_avulsa'] ?>">
						</figure>
						<h2><?php echo $configuracao['opt_titulo_orientacao_avulsa'] ?></h2>
					</a>
				</li>
			</ul>
		</section>
	</div>

	<section class="sessaoAgendaJurifelx" id="0">
		<h3><?php echo $configuracao['opt_titulo_agendamento_juriflex'] ?></h3>
		<p class="paddingCorrecao"><?php echo $configuracao['opt_desc_agendamento_juriflex'] ?></p>
		<ul class="instrucao">
			<li><?php echo $configuracao['opt_tempo_agendamento'] ?></li>
			<li><?php echo $configuracao['opt_agendamento_consulta_gratuita'] ?></li>
			<li><?php echo $configuracao['opt_agendamento_vamos_ate_voce'] ?></li>
		</ul>
		<div class="areaAgendamento">
			<div class="row">
				<div class="col-sm-8">
					<div class="carrossel-calendario">
						<div class="carrosselCalendario" id="carrosselCalendario0">
							<div class="item">

								<h5>Julho 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>30</span>

										<span disabled data-dia="07/07">07</span>

										<span disabled data-dia="14/07">14</span>

										<span disabled data-dia="21/07">21</span>

										<span disabled data-dia="28/07">28</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled data-dia="01/07">01</span>

										<span disabled data-dia="08/07">08</span>

										<span disabled data-dia="15/07">15</span>

										<span disabled data-dia="22/07">22</span>

										<span disabled data-dia="29/07">29</span>

									</li>

									<li>

										<p>Ter</p>

										<span data-dia="02/07">02</span>

										<span data-dia="09/07">09</span>

										<span data-dia="16/07">16</span>

										<span data-dia="23/07">23</span>

										<span data-dia="30/07">30</span>

									</li>

									<li>

										<p>Qua</p>

										<span data-dia="03/07">03</span>

										<span data-dia="10/07">10</span>

										<span data-dia="17/07">17</span>

										<span data-dia="24/07">24</span>

										<span data-dia="31/07">31</span>

									</li>

									<li>

										<p>Qui</p>

										<span data-dia="04/04">04</span>

										<span data-dia="11/04">11</span>

										<span data-dia="18/04">18</span>

										<span data-dia="25/04">25</span>

										<span disabled>01</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="05/04">05</span>

										<span data-dia="12/04">12</span>

										<span data-dia="19/04">19</span>

										<span data-dia="26/04">26</span>

										<span disabled>02</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="06/04">06</span>

										<span disabled data-dia="13/04">13</span>

										<span disabled data-dia="20/04">20</span>

										<span disabled data-dia="27/04">27</span>

										<span disabled>03</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Agosto 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>28</span>

										<span disabled data-dia="04/08">04</span>

										<span disabled data-dia="11/08">11</span>

										<span disabled data-dia="18/08">18</span>

										<span disabled data-dia="25/08">25</span>


									</li>

									<li>

										<p>Seg</p>

										<span disabled>29</span>

										<span disabled data-dia="05/08">05</span>

										<span disabled data-dia="12/08">12</span>

										<span disabled data-dia="19/08">19</span>

										<span disabled data-dia="26/08">26</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>30</span>

										<span data-dia="06/08">06</span>

										<span data-dia="13/08">13</span>

										<span data-dia="20/08">20</span>

										<span data-dia="27/08">27</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>31</span>

										<span data-dia="07/08">07</span>

										<span data-dia="14/08">14</span>

										<span data-dia="21/08">21</span>

										<span data-dia="28/08">28</span>

									</li>

									<li>

										<p>Qui</p>

										<span data-dia="01/08">01</span>

										<span data-dia="08/08">08</span>

										<span data-dia="15/08">15</span>

										<span data-dia="22/08">22</span>

										<span data-dia="28/08">28</span>


									</li>

									<li>

										<p>Sex</p>

										<span data-dia="02/08">02</span>

										<span data-dia="09/08">09</span>

										<span data-dia="16/08">16</span>

										<span data-dia="23/08">23</span>

										<span data-dia="29/08">29</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="03/08">03</span>

										<span disabled data-dia="10/08">10</span>

										<span disabled data-dia="17/08">17</span>

										<span disabled data-dia="24/08">24</span>

										<span disabled data-dia="31/08">31</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Setembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled data-dia="01/09">01</span>

										<span disabled data-dia="08/09">08</span>

										<span disabled data-dia="15/09">15</span>

										<span disabled data-dia="22/09">22</span>

										<span disabled data-dia="29/09">29</span>


									</li>

									<li>

										<p>Seg</p>

										<span disabled data-dia="02/09">02</span>

										<span disabled data-dia="09/09">09</span>

										<span disabled data-dia="16/09">16</span>

										<span disabled data-dia="23/09">23</span>

										<span disabled data-dia="30/09">30</span>

									</li>

									<li>

										<p>Ter</p>1

										<span data-dia="03/09">03</span>

										<span data-dia="10/09">10</span>

										<span data-dia="17/09">17</span>

										<span data-dia="24/09">24</span>

										<span disabled>01</span>

									</li>

									<li>

										<p>Qua</p>

										<span data-dia="04/09">04</span>

										<span data-dia="11/09">11</span>

										<span data-dia="18/09">18</span>

										<span data-dia="25/09">25</span>

										<span disabled>02</span>

									</li>

									<li>

										<p>Qui</p>


										<span data-dia="05/09">05</span>

										<span data-dia="12/09">12</span>

										<span data-dia="19/09">19</span>

										<span data-dia="26/09">26</span>

										<span disabled>03</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="06/09">06</span>

										<span data-dia="13/09">13</span>

										<span data-dia="20/09">20</span>

										<span data-dia="27/09">27</span>

										<span disabled>04</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="07/09">07</span>

										<span disabled data-dia="14/09">14</span>

										<span disabled data-dia="21/09">21</span>

										<span disabled data-dia="28/09">28</span>

										<span disabled>05</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Outubro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>29</span>

										<span disabled data-dia="06/10">06</span>

										<span disabled data-dia="13/10">13</span>

										<span disabled data-dia="20/10">20</span>

										<span disabled data-dia="27/10">27</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>30</span>

										<span disabled data-dia="07/10">07</span>

										<span disabled data-dia="14/10">14</span>

										<span disabled data-dia="21/10">21</span>

										<span disabled data-dia="28/10">28</span>

									</li>

									<li>

										<p>Ter</p>

										<span data-dia="01/10">01</span>

										<span data-dia="08/10">08</span>

										<span data-dia="15/10">15</span>

										<span data-dia="22/10">22</span>

										<span data-dia="28/10">29</span>


									</li>

									<li>

										<p>Qua</p>

										<span data-dia="02/10">02</span>

										<span data-dia="09/10">09</span>

										<span data-dia="16/10">16</span>

										<span data-dia="23/10">23</span>

										<span data-dia="29/10">30</span>

									</li>

									<li>

										<p>Qui</p>

										<span data-dia="03/10">03</span>

										<span data-dia="10/10">10</span>

										<span data-dia="17/10">17</span>

										<span data-dia="24/10">24</span>

										<span data-dia="31/10">31</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="04/10">04</span>

										<span data-dia="11/10">11</span>

										<span data-dia="18/10">18</span>

										<span data-dia="25/10">25</span>

										<span disabled>01</span>


									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="05/10">05</span>

										<span disabled data-dia="12/10">12</span>

										<span disabled data-dia="19/10">19</span>

										<span disabled data-dia="26/10">26</span>

										<span disabled>02</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Novembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>27</span>

										<span disabled data-dia="03/11">03</span>

										<span disabled data-dia="03/11">10</span>

										<span disabled data-dia="03/11">17</span>

										<span disabled data-dia="03/11">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>28</span>

										<span disabled data-dia="04/11">04</span>

										<span disabled data-dia="11/11">11</span>

										<span disabled data-dia="18/11">18</span>

										<span disabled data-dia="25/11">25</span>


									</li>

									<li>

										<p>Ter</p>

										<span disabled>29</span>

										<span data-dia="05/11">05</span>

										<span data-dia="12/11">12</span>

										<span data-dia="19/11">19</span>

										<span data-dia="26/11">26</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>30</span>

										<span data-dia="06/11">06</span>

										<span data-dia="13/11">13</span>

										<span data-dia="20/11">20</span>

										<span data-dia="27/11">27</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>31</span>

										<span data-dia="07/11">07</span>

										<span data-dia="14/11">14</span>

										<span data-dia="21/11">21</span>

										<span data-dia="28/11">28</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="01/11">01</span>

										<span data-dia="08/11">08</span>

										<span data-dia="15/11">15</span>

										<span data-dia="22/11">22</span>

										<span data-dia="28/11">29</span>


									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="02/11">02</span>

										<span disabled data-dia="09/11">09</span>

										<span disabled data-dia="16/11">16</span>

										<span disabled data-dia="23/11">23</span>

										<span disabled data-dia="29/11">30</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Dezembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled data-dia="01/12">01</span>

										<span disabled data-dia="08/12">08</span>

										<span disabled data-dia="15/12">15</span>

										<span disabled data-dia="22/12">22</span>

										<span disabled data-dia="29/12">29</span>


									</li>

									<li>

										<p>Seg</p>

										<span disabled data-dia="02/12">02</span>

										<span disabled data-dia="09/12">09</span>

										<span disabled data-dia="16/12">16</span>

										<span disabled data-dia="23/12">23</span>

										<span disabled data-dia="30/12">30</span>

									</li>

									<li>

										<p>Ter</p>

										<span data-dia="03/12">03</span>

										<span data-dia="10/12">10</span>

										<span data-dia="17/12">17</span>

										<span data-dia="24/12">24</span>

										<span data-dia="31/12">31</span>

									</li>

									<li>

										<p>Qua</p>

										<span data-dia="04/12">04</span>

										<span data-dia="11/12">11</span>

										<span data-dia="18/12">18</span>

										<span data-dia="25/12">25</span>

										<span disabled>01</span>


									</li>

									<li>

										<p>Qui</p>

										<span data-dia="05/12">05</span>

										<span data-dia="12/12">12</span>

										<span data-dia="19/12">19</span>

										<span data-dia="26/12">26</span>

										<span disabled>02</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="06/12">06</span>

										<span data-dia="13/12">13</span>

										<span data-dia="20/12">20</span>

										<span data-dia="27/12">27</span>

										<span disabled>03</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="07/12">07</span>

										<span disabled data-dia="14/12">14</span>

										<span disabled data-dia="21/12">21</span>

										<span disabled data-dia="28/12">28</span>

										<span disabled>04</span>

									</li>

								</ul>

							</div>
						</div>

						<!-- BOTÕES CARROSSEL CALENDÁRIO -->
						<div class="btnCarrossel">
							<button id="flechaEsquerda" class="esquerdacarrosselCalendario"><img src="<?php echo get_template_directory_uri() ?>/img/left.svg" alt="Flexas"></button>
							<button id="flechaDireita" class="direitacarrosselCalendario"><img src="<?php echo get_template_directory_uri() ?>/img/right.svg" alt="Flexas"></button>
						</div>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="areaFormulario">
						<img src="<?php echo get_template_directory_uri(); ?>/img/calendar.png" alt="calendar">
						<p class="p-agendamento">Selecione seu <br> Agendamento:</p>
						<?php echo do_shortcode('[contact-form-7 id="51" title="Formulário de agendamento(juriflex)"]'); ?>
					</div>
				</div>	
			</div>
		</div>
	</section>

	<section class="sessaoAgendaJurifelx" id="1">
		<h3>Orientação avulsa</h3>
		<p class="paddingCorrecao"><?php echo $configuracao['opt_desc_agendamento_juriflex'] ?></p>
		<!-- <ul class="instrucao">
			<li><?php //echo $configuracao['opt_tempo_agendamento'] ?></li>
			<li><?php //echo $configuracao['opt_agendamento_consulta_gratuita'] ?></li>
			<li><?php //echo $configuracao['opt_agendamento_vamos_ate_voce'] ?></li>
		</ul> -->
		<div class="areaAgendamento">
			<div class="row">
				<div class="col-sm-8">
					<div class="carrossel-calendario">
						<div class="carrosselCalendario" id="carrosselCalendario1">
							<div class="item">

								<h5>Julho 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>30</span>

										<span disabled data-dia="07/07">07</span>

										<span disabled data-dia="14/07">14</span>

										<span disabled data-dia="21/07">21</span>

										<span disabled data-dia="28/07">28</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled data-dia="01/07">01</span>

										<span disabled data-dia="08/07">08</span>

										<span disabled data-dia="15/07">15</span>

										<span disabled data-dia="22/07">22</span>

										<span disabled data-dia="29/07">29</span>

									</li>

									<li>

										<p>Ter</p>

										<span data-dia="02/07">02</span>

										<span data-dia="09/07">09</span>

										<span data-dia="16/07">16</span>

										<span data-dia="23/07">23</span>

										<span data-dia="30/07">30</span>

									</li>

									<li>

										<p>Qua</p>

										<span data-dia="03/07">03</span>

										<span data-dia="10/07">10</span>

										<span data-dia="17/07">17</span>

										<span data-dia="24/07">24</span>

										<span data-dia="31/07">31</span>

									</li>

									<li>

										<p>Qui</p>

										<span data-dia="04/04">04</span>

										<span data-dia="11/04">11</span>

										<span data-dia="18/04">18</span>

										<span data-dia="25/04">25</span>

										<span disabled>01</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="05/04">05</span>

										<span data-dia="12/04">12</span>

										<span data-dia="19/04">19</span>

										<span data-dia="26/04">26</span>

										<span disabled>02</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="06/04">06</span>

										<span disabled data-dia="13/04">13</span>

										<span disabled data-dia="20/04">20</span>

										<span disabled data-dia="27/04">27</span>

										<span disabled>03</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Agosto 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>28</span>

										<span disabled data-dia="04/08">04</span>

										<span disabled data-dia="11/08">11</span>

										<span disabled data-dia="18/08">18</span>

										<span disabled data-dia="25/08">25</span>


									</li>

									<li>

										<p>Seg</p>

										<span disabled>29</span>

										<span disabled data-dia="05/08">05</span>

										<span disabled data-dia="12/08">12</span>

										<span disabled data-dia="19/08">19</span>

										<span disabled data-dia="26/08">26</span>

									</li>

									<li>

										<p>Ter</p>

										<span disabled>30</span>

										<span data-dia="06/08">06</span>

										<span data-dia="13/08">13</span>

										<span data-dia="20/08">20</span>

										<span data-dia="27/08">27</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>31</span>

										<span data-dia="07/08">07</span>

										<span data-dia="14/08">14</span>

										<span data-dia="21/08">21</span>

										<span data-dia="28/08">28</span>

									</li>

									<li>

										<p>Qui</p>

										<span data-dia="01/08">01</span>

										<span data-dia="08/08">08</span>

										<span data-dia="15/08">15</span>

										<span data-dia="22/08">22</span>

										<span data-dia="28/08">28</span>


									</li>

									<li>

										<p>Sex</p>

										<span data-dia="02/08">02</span>

										<span data-dia="09/08">09</span>

										<span data-dia="16/08">16</span>

										<span data-dia="23/08">23</span>

										<span data-dia="29/08">29</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="03/08">03</span>

										<span disabled data-dia="10/08">10</span>

										<span disabled data-dia="17/08">17</span>

										<span disabled data-dia="24/08">24</span>

										<span disabled data-dia="31/08">31</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Setembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled data-dia="01/09">01</span>

										<span disabled data-dia="08/09">08</span>

										<span disabled data-dia="15/09">15</span>

										<span disabled data-dia="22/09">22</span>

										<span disabled data-dia="29/09">29</span>


									</li>

									<li>

										<p>Seg</p>

										<span disabled data-dia="02/09">02</span>

										<span disabled data-dia="09/09">09</span>

										<span disabled data-dia="16/09">16</span>

										<span disabled data-dia="23/09">23</span>

										<span disabled data-dia="30/09">30</span>

									</li>

									<li>

										<p>Ter</p>1

										<span data-dia="03/09">03</span>

										<span data-dia="10/09">10</span>

										<span data-dia="17/09">17</span>

										<span data-dia="24/09">24</span>

										<span disabled>01</span>

									</li>

									<li>

										<p>Qua</p>

										<span data-dia="04/09">04</span>

										<span data-dia="11/09">11</span>

										<span data-dia="18/09">18</span>

										<span data-dia="25/09">25</span>

										<span disabled>02</span>

									</li>

									<li>

										<p>Qui</p>


										<span data-dia="05/09">05</span>

										<span data-dia="12/09">12</span>

										<span data-dia="19/09">19</span>

										<span data-dia="26/09">26</span>

										<span disabled>03</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="06/09">06</span>

										<span data-dia="13/09">13</span>

										<span data-dia="20/09">20</span>

										<span data-dia="27/09">27</span>

										<span disabled>04</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="07/09">07</span>

										<span disabled data-dia="14/09">14</span>

										<span disabled data-dia="21/09">21</span>

										<span disabled data-dia="28/09">28</span>

										<span disabled>05</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Outubro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>29</span>

										<span disabled data-dia="06/10">06</span>

										<span disabled data-dia="13/10">13</span>

										<span disabled data-dia="20/10">20</span>

										<span disabled data-dia="27/10">27</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>30</span>

										<span disabled data-dia="07/10">07</span>

										<span disabled data-dia="14/10">14</span>

										<span disabled data-dia="21/10">21</span>

										<span disabled data-dia="28/10">28</span>

									</li>

									<li>

										<p>Ter</p>

										<span data-dia="01/10">01</span>

										<span data-dia="08/10">08</span>

										<span data-dia="15/10">15</span>

										<span data-dia="22/10">22</span>

										<span data-dia="28/10">29</span>


									</li>

									<li>

										<p>Qua</p>

										<span data-dia="02/10">02</span>

										<span data-dia="09/10">09</span>

										<span data-dia="16/10">16</span>

										<span data-dia="23/10">23</span>

										<span data-dia="29/10">30</span>

									</li>

									<li>

										<p>Qui</p>

										<span data-dia="03/10">03</span>

										<span data-dia="10/10">10</span>

										<span data-dia="17/10">17</span>

										<span data-dia="24/10">24</span>

										<span data-dia="31/10">31</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="04/10">04</span>

										<span data-dia="11/10">11</span>

										<span data-dia="18/10">18</span>

										<span data-dia="25/10">25</span>

										<span disabled>01</span>


									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="05/10">05</span>

										<span disabled data-dia="12/10">12</span>

										<span disabled data-dia="19/10">19</span>

										<span disabled data-dia="26/10">26</span>

										<span disabled>02</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Novembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

							 			<span disabled>27</span>

										<span disabled data-dia="03/11">03</span>

										<span disabled data-dia="03/11">10</span>

										<span disabled data-dia="03/11">17</span>

										<span disabled data-dia="03/11">24</span>

									</li>

									<li>

										<p>Seg</p>

										<span disabled>28</span>

										<span disabled data-dia="04/11">04</span>

										<span disabled data-dia="11/11">11</span>

										<span disabled data-dia="18/11">18</span>

										<span disabled data-dia="25/11">25</span>


									</li>

									<li>

										<p>Ter</p>

										<span disabled>29</span>

										<span data-dia="05/11">05</span>

										<span data-dia="12/11">12</span>

										<span data-dia="19/11">19</span>

										<span data-dia="26/11">26</span>

									</li>

									<li>

										<p>Qua</p>

										<span disabled>30</span>

										<span data-dia="06/11">06</span>

										<span data-dia="13/11">13</span>

										<span data-dia="20/11">20</span>

										<span data-dia="27/11">27</span>

									</li>

									<li>

										<p>Qui</p>

										<span disabled>31</span>

										<span data-dia="07/11">07</span>

										<span data-dia="14/11">14</span>

										<span data-dia="21/11">21</span>

										<span data-dia="28/11">28</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="01/11">01</span>

										<span data-dia="08/11">08</span>

										<span data-dia="15/11">15</span>

										<span data-dia="22/11">22</span>

										<span data-dia="28/11">29</span>


									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="02/11">02</span>

										<span disabled data-dia="09/11">09</span>

										<span disabled data-dia="16/11">16</span>

										<span disabled data-dia="23/11">23</span>

										<span disabled data-dia="29/11">30</span>

									</li>

								</ul>

							</div>
							<div class="item">

								<h5>Dezembro 2019</h5>

								<ul>

									<li>

										<p>Dom</p>

										<span disabled data-dia="01/12">01</span>

										<span disabled data-dia="08/12">08</span>

										<span disabled data-dia="15/12">15</span>

										<span disabled data-dia="22/12">22</span>

										<span disabled data-dia="29/12">29</span>


									</li>

									<li>

										<p>Seg</p>

										<span disabled data-dia="02/12">02</span>

										<span disabled data-dia="09/12">09</span>

										<span disabled data-dia="16/12">16</span>

										<span disabled data-dia="23/12">23</span>

										<span disabled data-dia="30/12">30</span>

									</li>

									<li>

										<p>Ter</p>

										<span data-dia="03/12">03</span>

										<span data-dia="10/12">10</span>

										<span data-dia="17/12">17</span>

										<span data-dia="24/12">24</span>

										<span data-dia="31/12">31</span>

									</li>

									<li>

										<p>Qua</p>

										<span data-dia="04/12">04</span>

										<span data-dia="11/12">11</span>

										<span data-dia="18/12">18</span>

										<span data-dia="25/12">25</span>

										<span disabled>01</span>


									</li>

									<li>

										<p>Qui</p>

										<span data-dia="05/12">05</span>

										<span data-dia="12/12">12</span>

										<span data-dia="19/12">19</span>

										<span data-dia="26/12">26</span>

										<span disabled>02</span>

									</li>

									<li>

										<p>Sex</p>

										<span data-dia="06/12">06</span>

										<span data-dia="13/12">13</span>

										<span data-dia="20/12">20</span>

										<span data-dia="27/12">27</span>

										<span disabled>03</span>

									</li>

									<li>

										<p>Sáb</p>

										<span disabled data-dia="07/12">07</span>

										<span disabled data-dia="14/12">14</span>

										<span disabled data-dia="21/12">21</span>

										<span disabled data-dia="28/12">28</span>

										<span disabled>04</span>

									</li>

								</ul>

							</div>
						</div>

						<!-- BOTÕES CARROSSEL CALENDÁRIO -->
						<div class="btnCarrossel">
							<button id="flechaEsquerda" class="esquerdacarrosselCalendario"><img src="<?php echo get_template_directory_uri() ?>/img/left.svg" alt="Flexas"></button>
							<button id="flechaDireita" class="direitacarrosselCalendario"><img src="<?php echo get_template_directory_uri() ?>/img/right.svg" alt="Flexas"></button>
						</div>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="areaFormulario">
						<img src="<?php echo get_template_directory_uri(); ?>/img/calendar.png" alt="calendar">
						<p class="p-agendamento">Selecione seu <br> Agendamento:</p>
						<?php echo do_shortcode('[contact-form-7 id="91" title="Fomulário de agendamento(orientação avulsa)"]'); ?>
					</div>
				</div>	
			</div>
		</div>
	</section>

	<!-- FORMULÁRIO DE CONTATO -->
	<?php include (TEMPLATEPATH . '/templates/formularioContato.php');  ?>
</div>

<?php get_footer();