<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package neubert_advogados
 */

get_header();
?>
<div class="pg pg-inicial">
	<div class="menuContato">
		<nav>
			<ul>
				<li class="whatsapp">
					<a href="https://api.whatsapp.com/send?phone=<?php echo $configuracao['opt_whatsapp'] ?>&text=Ol%C3%A1!" target="_blank">
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/whatsapp.png" alt="">
							<figcaption>whatsapp</figcaption>
						</figure>
					</a>
				</li>
				<li>
					<a href="<?php echo "#" ?>" class="chat">
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/chat.png" alt="">
							<figcaption>chat</figcaption>
						</figure>
					</a>
				</li>
				<li>
					<a href="<?php echo $configuracao['opt_facebook'] ?>">
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/user.png" alt="">
							<figcaption>login</figcaption>
						</figure>
					</a>
				</li>
			</ul>
		</nav>
	</div>
	<section class="sessaoDestaque">

		<h4 class="hidden">Área de destaque</h4>

		<div class="carrosselDestaque" id="carrosselDestaque">

			<?php 
				//LOOP DE POST DESTAQUE
				$destaque = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $destaque->have_posts() ) : $destaque->the_post();
					if(rwmb_meta('NubertAdvogados_destaque_checkbox')):
			 ?>

			<div class="item textToTheRight">
				<figure>
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden">Destaque</figcaption>
				</figure>
				<article>
					<?php echo the_content(); ?>
					<a href="<?php echo rwmb_meta('NubertAdvogados_destaque_link'); ?>" title="<?php echo get_the_title() ?>">Contrate Agora!</a>
				</article>
			</div>

			<?php else: ?>

			<div class="item">
				<figure>
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="hidden">Destaque</figcaption>
				</figure>
				<article>
					<?php echo the_content(); ?>
					<a href="<?php echo rwmb_meta('NubertAdvogados_destaque_link'); ?>" title="<?php echo get_the_title() ?>">Contrate Agora!</a>
				</article>
			</div>

			<?php endif; endwhile; wp_reset_query(); ?>
		</div>

		<div class="setasCarrosselDestaque">
			<button class="esquerdaCarrossel">
				<img src="<?php echo get_template_directory_uri() ?>/img/angle.svg" alt="Seta carrossel destaque">
			</button>
			<button class="direitaCarrossel">
				<img src="<?php echo get_template_directory_uri() ?>/img/angle.svg" alt="Seta carrossel destaque">
			</button>
		</div>
	</section>



	<section class="sessaoQuemSomos" id="previdenciario">

		<h4 class="hidden">Quem Somos</h4>



		<div class="row">

			<div class="col-sm-5">

				<figure class="imgIlustrtiva">

					<img src="<?php echo $configuracao['config_area_previsenciario_imagem']['url'] ?>" alt="<?php echo $configuracao['config_area_previsenciario_titulo'] ?>">

					<figcaption class="hidden"><?php echo $configuracao['config_area_previsenciario_titulo'] ?></figcaption>

				</figure>

			</div>

			<div class="col-sm-7">

				<div class="descricaoQuemSomos">

					<article>

						<h2><?php echo $configuracao['config_area_previsenciario_titulo'] ?></h2>

						<?php echo $configuracao['config_area_previsenciario_textto'] ?>

						<ul>

							<li>

								<figure>

									<img src="<?php echo $configuracao['config_area_previsenciario_icone_1']['url'] ?>" alt="<?php echo $configuracao['config_area_previsenciario_texto_1'] ?>">

									<figcaption class="hidden"><?php echo $configuracao['config_area_previsenciario_texto_1'] ?></figcaption>

									<h2><?php echo $configuracao['config_area_previsenciario_texto_1'] ?></h2>

								</figure>

							</li>



							<li>

								<figure>

									<img src="<?php echo $configuracao['config_area_previsenciario_icone_2']['url'] ?>" alt="<?php echo $configuracao['config_area_previsenciario_texto_2'] ?>">

									<figcaption class="hidden"><?php echo $configuracao['config_area_previsenciario_texto_2'] ?></figcaption>

									<h2><?php echo $configuracao['config_area_previsenciario_texto_2'] ?></h2>

								</figure>

							</li>



							<li>

								<figure>

									<img src="<?php echo $configuracao['config_area_previsenciario_icone_3']['url'] ?>" alt="<?php echo $configuracao['config_area_previsenciario_texto_3'] ?>">

									<figcaption class="hidden"><?php echo $configuracao['config_area_previsenciario_texto_3'] ?></figcaption>

									<h2><?php echo $configuracao['config_area_previsenciario_texto_3'] ?></h2>

								</figure>

							</li>



							<li>

								<figure>

									<img src="<?php echo $configuracao['config_area_previsenciario_icone_4']['url'] ?>" alt="<?php echo $configuracao['config_area_previsenciario_texto_4'] ?>">

									<figcaption class="hidden"><?php echo $configuracao['config_area_previsenciario_texto_4'] ?></figcaption>

									<h2><?php echo $configuracao['config_area_previsenciario_texto_4'] ?></h2>

								</figure>

							</li>



						</ul>



						<div class="realizeSeuPedido">

							<p><?php echo $configuracao['config_area_previsenciario_texto_pedido'] ?></p>

							<span><?php echo $configuracao['config_area_previsenciario_texto_pedido_desc'] ?></span>

							<a href="<?php echo $configuracao['config_area_previsenciario_texto_pedido_link'] ?>" class="hoverBtnPaginaInicial">Agende seu Horário</a>

						</div>

					</article>

				</div>

			</div>

		</div>

	</section>



	<section class="sessaoVantagens">

		<h4><?php echo $configuracao['config_area_vantagens_titulo'] ?></h4>

		<ul>

			<li>

				<figure>

					<img src="<?php echo $configuracao['config_area_vantagens_icone_1']['url'] ?>" alt="<?php echo $configuracao['config_area_vantagens_titulo_1'] ?>">

					<figcaption class="hidden"><?php echo $configuracao['config_area_vantagens_titulo_1'] ?></figcaption>

				</figure>

				<h2><?php echo $configuracao['config_area_vantagens_titulo_1'] ?></h2>

				<p><?php echo $configuracao['config_area_vantagens_texto_1'] ?></p>

			</li>



			<li>

				<figure>

					<img src="<?php echo $configuracao['config_area_vantagens_icone_2']['url'] ?>" alt="<?php echo $configuracao['config_area_vantagens_titulo_2'] ?>">

					<figcaption class="hidden"><?php echo $configuracao['config_area_vantagens_titulo_2'] ?></figcaption>

				</figure>

				<h2><?php echo $configuracao['config_area_vantagens_titulo_2'] ?></h2>

				<p><?php echo $configuracao['config_area_vantagens_texto_2'] ?></p>

			</li>



			<li>

				<figure>

					<img src="<?php echo $configuracao['config_area_vantagens_icone_3']['url'] ?>" alt="<?php echo $configuracao['config_area_vantagens_titulo_3'] ?>">

					<figcaption class="hidden"><?php echo $configuracao['config_area_vantagens_titulo_3'] ?></figcaption>

				</figure>

				<h2><?php echo $configuracao['config_area_vantagens_titulo_3'] ?></h2>

				<p><?php echo $configuracao['config_area_vantagens_texto_3'] ?></p>

			</li>





		</ul>

	</section>



	<section class="sessaoJuriflex" id="juriflex">

		<h4 class="hidden"><?php echo $configuracao['config_area_Juriflex_titulo'] ?></h4>



		<div class="container">

			

			<div class="row">

				<div class="col-sm-5">

				

					<article>

					<?php echo $configuracao['config_area_Juriflex_texto'] ?>

					</article>

				

				</div>

				<div class="col-sm-7">

					

					<div class="video">

						<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $configuracao['config_area_Juriflex_video'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

					</div>



				</div>

			</div>



			<div class="areaAgenda">

				<p><?php echo $configuracao['config_area_Juriflex_texto_pedido'] ?></p>

				<span><?php echo $configuracao['config_area_Juriflex_texto_pedido_desc'] ?></span>

				<a href="<?php echo $configuracao['config_area_Juriflex_texto_pedido_link'] ?>" class="hoverBtnPaginaInicial">Agende seu Horário</a>

			</div>

		

		 </div>



		

	</section>



	<section class="sessaoServicos" id="servicos">
		<h4 class="hidden">Serviços</h4>
		<div class="areaServicos" id="carrosselServico">
			<?php 
				$contator = 0;
				//LOOP DE POST SERVIÇOS
				$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $servicos->have_posts() ) : $servicos->the_post();
					if($contador == 0):
			 ?>

			<div class="item item-ativo" data-id="0">
				<figure>
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption ><?php echo get_the_title() ?></figcaption>
				</figure>
				<h2><?php echo get_the_title() ?></h2>
			</div>

			<?php else: ?>

			<div class="item" data-id="<?php echo $contador; ?>">
				<figure>
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption ><?php echo get_the_title() ?></figcaption>
				</figure>
				<h2><?php echo get_the_title() ?></h2>
			</div>

			<?php endif; $contador++; endwhile; wp_reset_query(); ?>
		</div>
	</section>

	<section class="sessaoEmpresarial">
		<?php 
		$contator1 = 0;
		//LOOP DE POST SERVIÇOS
		$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
		while ( $servicos->have_posts() ) : $servicos->the_post();
			if($contador1 == 0):
		?>
		<div class="single-servico servico-ativo servico-translate-left" id="0">
			<div class="container containerEmpresarial">
				<h4><?php echo get_the_title(); ?></h4>
				<?php echo get_the_content(); ?>
				<a href="<?php echo rwmb_meta('NubertAdvogados_servico_link_botao'); ?>" class="hoverBtnPaginaInicial">Agende sua orientação</a>
			</div>
		</div>
		<?php else: ?>
		<div class="single-servico" id="<?php echo $contador1; ?>">
			<div class="container containerEmpresarial">
				<h4><?php echo get_the_title(); ?></h4>
				<?php echo get_the_content(); ?>
				<a href="<?php echo rwmb_meta('NubertAdvogados_servico_link_botao'); ?>" class="hoverBtnPaginaInicial">Agende sua orientação</a>
			</div>
		</div>
		<?php endif; $contador1++; endwhile; wp_reset_query(); ?>
	</section>
	
	<div id="map" style="height: 610px; margin-bottom: 80px;"></div>

	<!-- FORMULÁRIO DE CONTATO -->
	<?php include (TEMPLATEPATH . '/templates/formularioContato.php');  ?>
</div>

<script>
	let icon = "<?php echo get_template_directory_uri() ?>/img/pin.png";

	function initMap() {

	//MAPA
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -25.436218, lng: -49.308784},
		zoom: 16,
		disableDefaultUI: true,
		zoomControl: true,
		scrollwheel: false,
		draggable: true, 
		disableDoubleClickZoom: true,
		styles: [
		{
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#f5f5f5"
			}
			]
		},
		{
			"elementType": "labels.icon",
			"stylers": [
			{
				"visibility": "off"
			}
			]
		},
		{
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#616161"
			}
			]
		},
		{
			"elementType": "labels.text.stroke",
			"stylers": [
			{
				"color": "#f5f5f5"
			}
			]
		},
		{
			"featureType": "administrative.land_parcel",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#bdbdbd"
			}
			]
		},
		{
			"featureType": "landscape",
			"elementType": "geometry.fill",
			"stylers": [
			{
				"color": "#eaeaea"
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "landscape.man_made",
			"elementType": "geometry.fill",
			"stylers": [
			{
				"color": "#eaeaea"
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "landscape.man_made",
			"elementType": "geometry.stroke",
			"stylers": [
			{
				"color": "#666666"
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "poi",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#eeeeee"
			}
			]
		},
		{
			"featureType": "poi",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#757575"
			}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#e5e5e5"
			}
			]
		},
		{
			"featureType": "poi.park",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#9e9e9e"
			}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#ffffff"
			}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry.stroke",
			"stylers": [
			{
				"color": "#cecece"
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "road.arterial",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#757575"
			}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#dadada"
			}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry.fill",
			"stylers": [
			{
				"color": "#bebebe"
			},
			{
				"visibility": "on"
			}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "geometry.stroke",
			"stylers": [
			{
				"color": "#a3a3a3"
			},
			{
				"visibility": "on"
			},
			{
				"weight": 2.5
			}
			]
		},
		{
			"featureType": "road.highway",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#616161"
			}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#9e9e9e"
			}
			]
		},
		{
			"featureType": "transit.line",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#e5e5e5"
			}
			]
		},
		{
			"featureType": "transit.station",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#eeeeee"
			}
			]
		},
		{
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [
			{
				"color": "#c9c9c9"
			}
			]
		},
		{
			"featureType": "water",
			"elementType": "labels.text.fill",
			"stylers": [
			{
				"color": "#9e9e9e"
			}
			]
		}
		]
	});
	//MARKERS OU PINS DO MAPA
	var markerPierDoVictor = new google.maps.Marker({
		position: {lat: -25.436218, lng: -49.308784},
		icon: icon,
		map: map
	});
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXrpmFKL6aBNN9D1TiNjLLNeJDqtoqNpU&callback=initMap" async defer></script>

<?php get_footer();