<?php



/**

 * Template Name: Quem Somos

 * Description: Página Quem Somos

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package neubert_advogados

 */



get_header();

?>



<div class="pg quem-somos">



	<div class="menuContato">

		<nav>

			<ul>
				<li class="whatsapp">
					<a href="https://api.whatsapp.com/send?phone=<?php echo $configuracao['opt_whatsapp'] ?>&text=Ol%C3%A1!" target="_blank">
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/whatsapp.png" alt="">
							<figcaption>whatsapp</figcaption>
						</figure>
					</a>
				</li>
				<li>
					<a href="<?php echo "#" ?>" class="chat">
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/chat.png" alt="">
							<figcaption>chat</figcaption>
						</figure>
					</a>
				</li>
				<li>
					<a href="<?php echo $configuracao['opt_facebook'] ?>">
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/user.png" alt="">
							<figcaption>login</figcaption>
						</figure>
					</a>
				</li>
			</ul>
		</nav>

	</div>



	<div class="areaQuemSomosTextosValores">

		<section class="areaQuemSomos">

			<h4 class="hidden">Quem somos</h4>

			<article  class="containerLarge paddingCorrecao">

				<h2><?php echo get_the_title(); ?></h2>

				<p><?php echo $configuracao['opt_desc_quem_somos'] ?></p>

			</article>

		</section>



		<section class="sessaoMissaoVisao paddingCorrecao">

			<h4 class="hidden">Valores da empresa</h4>



			<ul class="containerLarge">

				<li>

					<img src="<?php echo $configuracao['opt_imagem_missao']['url'] ?>" alt="">

					<h3><?php echo $configuracao['opt_titulo_missao'] ?> </h3>

					<p><?php echo $configuracao['opt_desc_missao'] ?></p>

				</li>

				<li>

					<img src="<?php echo $configuracao['opt_imagem_visao']['url'] ?>" alt="">

					<h3><?php echo $configuracao['opt_titulo_visao'] ?> </h3>

					<p><?php echo $configuracao['opt_desc_visao'] ?></p>

				</li>

				<li>

					<img src="<?php echo $configuracao['opt_imagem_valores']['url'] ?>" alt="">

					<h3><?php echo $configuracao['opt_titulo_valores'] ?> </h3>

					<p><?php echo $configuracao['opt_desc_valores'] ?></p>

				</li>

			</ul>

		</section>

	</div>



	<section class="sessaoEquipe ">

		<h4><?php echo $configuracao['opt_titulo_profissionais'] ?></h4>

		<ul>

			<?php

				$profissionais = new WP_Query( array('post_type' => 'profissional', 'orderby' => 'id', 'posts_per_page' => -1, 'order' => 'asc') );

				while($profissionais->have_posts()): $profissionais->the_post();

					$foto_profissional = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

					$foto_profissional = $foto_profissional[0];

			?>

			<li>

				<figure>

					<img src="<?php echo $foto_profissional; ?>" alt="Imagem <?php echo the_title(); ?>">

					<figcaption><?php echo get_the_title(); ?></figcaption>	

				</figure>

				<h2><?php echo get_the_title(); ?></h2>

			</li>

			<?php endwhile; wp_reset_query(); ?>

		</ul>

	</section>

	

	<!-- FORMULÁRIO DE CONTATO -->

	<?php include (TEMPLATEPATH . '/templates/formularioContato.php');  ?>



</div>



<?php get_footer();