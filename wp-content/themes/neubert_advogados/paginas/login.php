<?php
/**
 * Template Name: Login
 * Description: Página Login
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package neubert_advogados
 */
global $configuracao;
?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<div class="pg pg-login">
		<div class="tiny-container">
			<figure class="logo">
				<img src="<?php echo get_template_directory_uri() ?>/img/logoMarca.png" alt="Logo Neubert Advogados">
				<figcaption class="hidden">Logo Neubert Advogados</figcaption>
			</figure>
			<figure class="usuario">
				<img src="<?php echo get_template_directory_uri() ?>/img/login.png" alt="Imagem usuário">
				<figcaption class="hidden">Imagem usuário</figcaption>
			</figure>
			<p>Faça seu login</p>
			<form name='form' method='post' action='http://www.siteadv.com.br/neubertadv/ExternalLogin.aspx?id=3518' class="login">
				<input type='email' name='login' placeholder="e-mail">
				<input type='password' name='senha' placeholder="senha">
				<label for="input-checkbox" style="display: none;">
					<input type="checkbox" id="input-checkbox" name="input-checkbox">
					<span class="label-input-checkbox">Mantenha-me conectado</span>
				</label>
				<div class="row">
					<div class="col-sm-6">
						<input type='submit' value='Entrar'>
					</div>
					<div class="col-sm-6">
						<input type='submit' value='Cadastrar-se' style="display: none;">
					</div>
				</div>
			</form>
			<a href="#" class="esqueceu-senha">Esqueceu sua senha?</a>
		</div>
	</div>

	<footer>
		<div class="copyright">
			<div class="row">
				<div class="col-sm-6">
					<div class="redesSociais_contato">
						<a href="<?php echo $configuracao['opt_youtube']?>" target="_blank"><b><i class="fab fa-linkedin-in"></i></b></a>
						<a href="<?php echo $configuracao['opt_instagram']?>" class="instagram" target="_blank"><b><i class="fab fa-instagram"></i></b></a>
						<a href="<?php echo $configuracao['opt_facebook']?>" target="_blank"><b><i class="fab fa-facebook-f"></i></b></a>
					</div>
				</div>
				<div class="col-sm-6">
					<p>Todos os direitos reservados - Neubert Advogados</p>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>
	<!-- begin olark code -->
	<!-- <script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
		/* custom configuration goes here (www.olark.com/documentation) */
		olark.identify('1635-449-10-8633');
	</script> -->
	<!-- end olark code -->
</body>
</html>