<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package neubert_advogados
 */

global $configuracao;

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<?php wp_head(); ?>

</head>



<body <?php body_class(); ?>>



<header class="topo">

	<div class="btnAbrirMenuMobile">

		<span></span>

		<span></span>

		<span></span>

	</div>
	<div class="menuPrincipal">
		<div class="containerTopo">
			<div class="row">
				<div class="col-sm-3">
					<a href="<?php echo get_home_url(); ?>" class="logoMarca">
						<figure style="background-image: url(<?php echo $configuracao['opt_logo']['url'] ?>)">
							<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php bloginfo( 'name' ); ?>">
							<figcaption class="hidden"><?php bloginfo( 'name' ); ?></figcaption>
						</figure>
					</a>
				</div>	
				<div class="col-sm-9">
					<div class="redesSociais_contato">
						<a href="tel:<?php echo $configuracao['opt_contato'] ?>"><span><?php echo $configuracao['opt_contato'] ?></span></a>
						<a href="<?php echo $configuracao['opt_youtube']?>" target="_blank"><b><i class="fab fa-linkedin-in"></i></b></a>
						<a href="<?php echo $configuracao['opt_instagram']?>" class="instagram" target="_blank"><b><i class="fab fa-instagram"></i></b></a>
						<a href="<?php echo $configuracao['opt_facebook']?>" target="_blank"><b><i class="fab fa-facebook-f"></i></b></a>
					</div>
					<nav>
						<?php 
						global $wp;
						$current_url = home_url( add_query_arg( array(), $wp->request ) ); 

						if ($current_url != home_url()) {
							$menu = array(
								'theme_location'  => '',
								'menu'            => 'Menu pages',
								'container'       => false,
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 2,
								'walker'          => ''
							);
							wp_nav_menu( $menu );
						}
						else{
							wp_nav_menu();
						}
						?>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="menuMobile">

		<span class="btnFecharMenuMobile">x</span>

		<div class="redesSociais_contato">

			<a href="tel:<?php echo $configuracao['opt_contato'] ?>"><span><?php echo $configuracao['opt_contato'] ?></span></a>

			<a href="<?php echo $configuracao['opt_youtube']?>" target="_blank"><b><i class="fab fa-linkedin-in"></i></b></a>

			<a href="<?php echo $configuracao['opt_instagram']?>" class="instagram" target="_blank"><b><i class="fab fa-instagram"></i></b></a>

			<a href="<?php echo $configuracao['opt_facebook']?>" target="_blank"><b><i class="fab fa-facebook-f"></i></b></a>

		</div>



		<nav>

			<?php wp_nav_menu(); ?>

		</nav>

	</div>

</header>