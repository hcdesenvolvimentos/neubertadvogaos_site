-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 13/07/2019 às 20:25
-- Versão do servidor: 5.7.26
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `hcdesenv_neubertadvogados`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_commentmeta`
--

CREATE TABLE `nb_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_comments`
--

CREATE TABLE `nb_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_comments`
--

INSERT INTO `nb_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-31 18:08:56', '2019-05-31 21:08:56', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_links`
--

CREATE TABLE `nb_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_options`
--

CREATE TABLE `nb_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_options`
--

INSERT INTO `nb_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://neubertadvogados.hcdesenvolvimentos.com', 'yes'),
(2, 'home', 'http://neubertadvogados.hcdesenvolvimentos.com', 'yes'),
(3, 'blogname', 'Neubert Advogados', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:162:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:11:\"servicos/?$\";s:27:\"index.php?post_type=servico\";s:41:\"servicos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=servico&feed=$matches[1]\";s:36:\"servicos/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=servico&feed=$matches[1]\";s:28:\"servicos/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=servico&paged=$matches[1]\";s:15:\"profissional/?$\";s:32:\"index.php?post_type=profissional\";s:45:\"profissional/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=profissional&feed=$matches[1]\";s:40:\"profissional/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?post_type=profissional&feed=$matches[1]\";s:32:\"profissional/page/([0-9]{1,})/?$\";s:50:\"index.php?post_type=profissional&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"servicos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"servicos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"servicos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"servicos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"servicos/([^/]+)/embed/?$\";s:40:\"index.php?servico=$matches[1]&embed=true\";s:29:\"servicos/([^/]+)/trackback/?$\";s:34:\"index.php?servico=$matches[1]&tb=1\";s:49:\"servicos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?servico=$matches[1]&feed=$matches[2]\";s:44:\"servicos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?servico=$matches[1]&feed=$matches[2]\";s:37:\"servicos/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?servico=$matches[1]&paged=$matches[2]\";s:44:\"servicos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?servico=$matches[1]&cpage=$matches[2]\";s:33:\"servicos/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?servico=$matches[1]&page=$matches[2]\";s:25:\"servicos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"servicos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"servicos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"servicos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:40:\"profissional/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"profissional/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"profissional/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"profissional/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"profissional/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"profissional/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"profissional/([^/]+)/embed/?$\";s:45:\"index.php?profissional=$matches[1]&embed=true\";s:33:\"profissional/([^/]+)/trackback/?$\";s:39:\"index.php?profissional=$matches[1]&tb=1\";s:53:\"profissional/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?profissional=$matches[1]&feed=$matches[2]\";s:48:\"profissional/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?profissional=$matches[1]&feed=$matches[2]\";s:41:\"profissional/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?profissional=$matches[1]&paged=$matches[2]\";s:48:\"profissional/([^/]+)/comment-page-([0-9]{1,})/?$\";s:52:\"index.php?profissional=$matches[1]&cpage=$matches[2]\";s:37:\"profissional/([^/]+)(?:/([0-9]+))?/?$\";s:51:\"index.php?profissional=$matches[1]&page=$matches[2]\";s:29:\"profissional/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"profissional/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"profissional/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"profissional/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"profissional/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"profissional/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:54:\"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:35:\"categoria-destaque/([^/]+)/embed/?$\";s:50:\"index.php?categoriaDestaque=$matches[1]&embed=true\";s:47:\"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]\";s:29:\"categoria-destaque/([^/]+)/?$\";s:39:\"index.php?categoriaDestaque=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=8&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:49:\"base-neubert_advogados/base-neubert_advogados.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:21:\"meta-box/meta-box.php\";i:4;s:28:\"olark-live-chat/olark-wp.php\";i:5;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'neubert_advogados', 'yes'),
(41, 'stylesheet', 'neubert_advogados', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '8', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'nb_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:4:{i:1563034137;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1563052137;a:4:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1563052157;a:3:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(916, '_site_transient_timeout_theme_roots', '1563035235', 'no'),
(917, '_site_transient_theme_roots', 'a:2:{s:17:\"neubert_advogados\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";}', 'no'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(689, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:6:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"wp_block\";s:4:\"show\";s:8:\"destaque\";s:4:\"show\";s:7:\"servico\";s:4:\"show\";s:12:\"profissional\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(121, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1559337205;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(573, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1563033434;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(574, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1563033435;s:7:\"checked\";a:2:{s:17:\"neubert_advogados\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(399, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:7:\"version\";s:5:\"5.2.2\";s:9:\"timestamp\";i:1560896153;}', 'no'),
(918, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1563033435;s:7:\"checked\";a:6:{s:49:\"base-neubert_advogados/base-neubert_advogados.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.3\";s:21:\"meta-box/meta-box.php\";s:6:\"4.18.2\";s:28:\"olark-live-chat/olark-wp.php\";s:5:\"1.0.9\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.4.1\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";}s:8:\"response\";a:1:{s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.18.4\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.18.4.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:28:\"olark-live-chat/olark-wp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/olark-live-chat\";s:4:\"slug\";s:15:\"olark-live-chat\";s:6:\"plugin\";s:28:\"olark-live-chat/olark-wp.php\";s:11:\"new_version\";s:5:\"1.0.9\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/olark-live-chat/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/olark-live-chat.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/olark-live-chat/assets/icon-256x256.jpg?rev=1731891\";s:2:\"1x\";s:68:\"https://ps.w.org/olark-live-chat/assets/icon-128x128.jpg?rev=1731891\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/olark-live-chat/assets/banner-1544x500.jpg?rev=1653767\";s:2:\"1x\";s:70:\"https://ps.w.org/olark-live-chat/assets/banner-772x250.jpg?rev=1653767\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.4.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.4.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(690, 'CPT_configured', 'TRUE', 'yes'),
(691, 'categoriaDestaque_children', 'a:0:{}', 'yes'),
(139, 'can_compress_scripts', '1', 'no'),
(142, 'recently_activated', 'a:0:{}', 'yes'),
(157, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1559326286;s:7:\"version\";s:5:\"5.1.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(862, '_site_transient_timeout_browser_bcf1814caa6afe84eeebef28ff236a7f', '1563372438', 'no'),
(863, '_site_transient_browser_bcf1814caa6afe84eeebef28ff236a7f', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"75.0.3770.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(864, '_site_transient_timeout_php_check_a5907c2ea4d6fbd7e531b3aa7734f0e4', '1563372438', 'no'),
(865, '_site_transient_php_check_a5907c2ea4d6fbd7e531b3aa7734f0e4', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:1;}', 'no'),
(378, 'olark-wp', 'a:9:{s:13:\"olark_site_ID\";s:16:\"1635-449-10-8633\";s:12:\"enable_olark\";i:1;s:16:\"enable_cartsaver\";i:0;s:14:\"start_expanded\";i:0;s:13:\"detached_chat\";i:0;s:13:\"override_lang\";i:0;s:10:\"olark_lang\";s:0:\"\";s:9:\"olark_api\";s:0:\"\";s:12:\"olark_mobile\";i:1;}', 'yes'),
(163, 'current_theme', 'Neubert Advogaos', 'yes'),
(164, 'theme_mods_neubert_advogados', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:6:\"menu-1\";i:2;s:11:\"menumariana\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(165, 'theme_switched', '', 'yes'),
(166, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(167, 'r_notice_data', '{\"type\":\"redux-message\",\"title\":\"<strong>Redux v4 Beta:  We Need Your Help!<\\/strong><br\\/>\\r\\n\\r\\n\",\"message\":\"The long in-development Redux v4 Beta is nearing completion and we could really use your help!  To learn how to do so, please read our latest blog post: <a href=\\\"https:\\/\\/reduxframework.com\\/2018\\/11\\/redux-4-0-we-need-your-help\\/\\\">Redux 4.0 - We Need Your Help!<\\/a>\",\"color\":\"rgba(0,162,227,1)\"}\n', 'yes'),
(168, 'redux_blast', '1559337262', 'yes'),
(169, 'configuracao', 'a:70:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:82:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/07/logo.png\";s:2:\"id\";s:2:\"93\";s:6:\"height\";s:3:\"114\";s:5:\"width\";s:3:\"162\";s:9:\"thumbnail\";s:90:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/07/logo-150x114.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"opt_logo_rodape\";a:9:{s:3:\"url\";s:88:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/logoRodape.png\";s:2:\"id\";s:2:\"57\";s:6:\"height\";s:3:\"139\";s:5:\"width\";s:3:\"199\";s:9:\"thumbnail\";s:96:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/logoRodape-150x139.png\";s:5:\"title\";s:10:\"logoRodape\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"rodape_titulo_texto\";s:122:\"<span><em>\"Diz o Senhor: amai a verdade e a paz.\" (Zc 8.19)</em> | </span>Todos os direitos reservados - Neubert Advogados\";s:12:\"opt_facebook\";s:42:\"https://www.facebook.com/NeubertAdvogados/\";s:11:\"opt_youtube\";s:55:\"https://www.linkedin.com/in/alexandre-neubert-26b0a217a\";s:13:\"opt_instagram\";s:43:\"https://www.instagram.com/alexandreneubert/\";s:12:\"opt_whatsapp\";s:13:\"5541999628333\";s:12:\"opt_endereco\";s:59:\"Rua General Mário Tourinho, 1805 | sala 605| Curitiba | PR\";s:11:\"opt_contato\";s:16:\"+55 41 3245 2402\";s:19:\"opt_desc_quem_somos\";s:358:\"<p>A Neubert Advogados é uma boutique jurídica que atua há mais de 10 anos em diferentes<br>áreas do Direito.Nossa equipe de profissionais possui alta especialização, atuação personalizada<br>e rápida no campo Empresarial; Previdenciário; Trabalhista; Cível e Tributário; além de oferecer<br>assessoria na esfera da Saúde e de Inventários.</p>\";s:17:\"opt_imagem_missao\";a:9:{s:3:\"url\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q2.png\";s:2:\"id\";s:2:\"27\";s:6:\"height\";s:2:\"66\";s:5:\"width\";s:2:\"67\";s:9:\"thumbnail\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q2.png\";s:5:\"title\";s:2:\"q2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"opt_imagem_visao\";a:9:{s:3:\"url\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q3.png\";s:2:\"id\";s:2:\"28\";s:6:\"height\";s:2:\"66\";s:5:\"width\";s:2:\"67\";s:9:\"thumbnail\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q3.png\";s:5:\"title\";s:2:\"q3\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:18:\"opt_imagem_valores\";a:9:{s:3:\"url\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q1.png\";s:2:\"id\";s:2:\"42\";s:6:\"height\";s:2:\"66\";s:5:\"width\";s:2:\"67\";s:9:\"thumbnail\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q1.png\";s:5:\"title\";s:2:\"q1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"opt_titulo_missao\";s:7:\"missão\";s:16:\"opt_titulo_visao\";s:6:\"visão\";s:18:\"opt_titulo_valores\";s:7:\"valores\";s:15:\"opt_desc_missao\";s:89:\"Neubert Advogados existe para garantir a seus clientes suporte jurídico ético e eficaz.\";s:14:\"opt_desc_visao\";s:137:\"Nossa meta é promover a excelência em diferentes áreas do Direito, personalizando os atendimentos com o uso de recursos tecnológicos.\";s:16:\"opt_desc_valores\";s:122:\"Compromisso, integridade e transparência guiam nossa atuação, sendo estes valores parte de nossas práticas cotidianas.\";s:24:\"opt_titulo_profissionais\";s:13:\"Profissionais\";s:33:\"config_area_previsenciario_imagem\";a:9:{s:3:\"url\";s:96:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/fotoPrevidenciario.png\";s:2:\"id\";s:2:\"88\";s:6:\"height\";s:3:\"715\";s:5:\"width\";s:3:\"866\";s:9:\"thumbnail\";s:104:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/fotoPrevidenciario-150x150.png\";s:5:\"title\";s:18:\"fotoPrevidenciario\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:33:\"config_area_previsenciario_titulo\";s:15:\"Previdenciário\";s:33:\"config_area_previsenciario_textto\";s:732:\"<h3>Somos especialistas em <br><strong>Seguridade Social</strong></h3>\r\n<p><strong>Oferecemos suporte para as diversas espécies de aposentadorias</strong><br>A Neubert Advogados atua no Direito Previdenciário há mais de 10 anos. Temos conhecimento técnico e experiência na estruturação da Previdência Pública e Privada brasileira, bem como na organização das normas e regulamentações da Seguridade Social.</p>\r\n\r\n<h3>Saiba <strong>quando e como </strong> solicitar<br> a aposentadoria</h3>\r\n<p>Ajudamos você a compreender os requisitos necessários para seu pedido de aposentadoria.<br>Além disso, damos todo o suporte para os cálculos e formas de obtê-la.<br>Conte com a nossa equipe para obter seu benefício:</p>\";s:34:\"config_area_previsenciario_icone_1\";a:9:{s:3:\"url\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/1.png\";s:2:\"id\";s:2:\"17\";s:6:\"height\";s:2:\"74\";s:5:\"width\";s:2:\"71\";s:9:\"thumbnail\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/1.png\";s:5:\"title\";s:1:\"1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:34:\"config_area_previsenciario_texto_1\";s:20:\"Contagem<br>de Tempo\";s:34:\"config_area_previsenciario_icone_2\";a:9:{s:3:\"url\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/4.png\";s:2:\"id\";s:2:\"20\";s:6:\"height\";s:2:\"74\";s:5:\"width\";s:2:\"71\";s:9:\"thumbnail\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/4.png\";s:5:\"title\";s:1:\"4\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:34:\"config_area_previsenciario_texto_2\";s:29:\"Planejamento Previdenciário \";s:34:\"config_area_previsenciario_icone_3\";a:9:{s:3:\"url\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/2.png\";s:2:\"id\";s:2:\"18\";s:6:\"height\";s:2:\"74\";s:5:\"width\";s:2:\"71\";s:9:\"thumbnail\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/2.png\";s:5:\"title\";s:1:\"2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:34:\"config_area_previsenciario_texto_3\";s:29:\"Requerimento de Aposentadoria\";s:34:\"config_area_previsenciario_icone_4\";a:9:{s:3:\"url\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/3.png\";s:2:\"id\";s:2:\"19\";s:6:\"height\";s:2:\"74\";s:5:\"width\";s:2:\"71\";s:9:\"thumbnail\";s:79:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/3.png\";s:5:\"title\";s:1:\"4\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:34:\"config_area_previsenciario_texto_4\";s:22:\"Revisão do Benefício\";s:39:\"config_area_previsenciario_texto_pedido\";s:52:\"Realize seu <strong>pedido de aposentadoria</strong>\";s:44:\"config_area_previsenciario_texto_pedido_desc\";s:55:\"Faça a solicitação ou revisão dos valores recebidos\";s:44:\"config_area_previsenciario_texto_pedido_link\";s:54:\"http://neubertadvogados.hcdesenvolvimentos.com/agenda/\";s:28:\"config_area_vantagens_titulo\";s:9:\"VANTAGENS\";s:29:\"config_area_vantagens_icone_1\";a:9:{s:3:\"url\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v3.png\";s:2:\"id\";s:2:\"46\";s:6:\"height\";s:2:\"99\";s:5:\"width\";s:3:\"127\";s:9:\"thumbnail\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v3.png\";s:5:\"title\";s:2:\"v3\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:30:\"config_area_vantagens_titulo_1\";s:36:\"AGILIDADE E ATENDIMENTO DIFERENCIADO\";s:29:\"config_area_vantagens_texto_1\";s:57:\"Suporte multidisciplinar e personalizado para o seu caso.\";s:29:\"config_area_vantagens_icone_2\";a:9:{s:3:\"url\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v1.png\";s:2:\"id\";s:2:\"44\";s:6:\"height\";s:2:\"99\";s:5:\"width\";s:3:\"127\";s:9:\"thumbnail\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v1.png\";s:5:\"title\";s:2:\"v1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:30:\"config_area_vantagens_titulo_2\";s:26:\"MELHOR<br>CUSTO-BENEFÍCIO\";s:29:\"config_area_vantagens_texto_2\";s:61:\"Valores compatíveis com a sua possibilidade de investimento.\";s:29:\"config_area_vantagens_icone_3\";a:9:{s:3:\"url\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v2.png\";s:2:\"id\";s:2:\"45\";s:6:\"height\";s:2:\"99\";s:5:\"width\";s:3:\"127\";s:9:\"thumbnail\";s:80:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v2.png\";s:5:\"title\";s:2:\"v2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:30:\"config_area_vantagens_titulo_3\";s:10:\"CONFIANÇA\";s:29:\"config_area_vantagens_texto_3\";s:65:\"Conte com nossa capacidade técnica, transparência e segurança.\";s:27:\"config_area_Juriflex_titulo\";s:51:\"Contrate a Juriflex Acessoria Jurídica Empresarial\";s:26:\"config_area_Juriflex_texto\";s:572:\"<h2>Contrate a <strong>Juriflex</strong> Acessoria Jurídica Empresarial</h2>\r\n<h3>Criada para negócios e organizações</h3><p>\r\nA Neubert Advogados desenvolveu um modelo de contratação focado em empresas, a Juriflex Assessoria Jurídica Empresarial. JURIFLEX é um modelo pay per use de contratação de honorários advocatícios com base em aquisição de créditos. O cliente identifica previamente os serviços que a empresa vai precisar no curto, médio e longo prazo, seguindo nossa Tabela, e assim escolhe quanto irá pagar por mês para ter direito a eles.</p>\";s:26:\"config_area_Juriflex_video\";s:11:\"nIA60MMMH2A\";s:33:\"config_area_Juriflex_texto_pedido\";s:51:\"Invista em uma <strong>assessoria completa</strong>\";s:38:\"config_area_Juriflex_texto_pedido_desc\";s:68:\"Garanta o suporte jurídico em todos os âmbitos de<br>seu negócio.\";s:38:\"config_area_Juriflex_texto_pedido_link\";s:54:\"http://neubertadvogados.hcdesenvolvimentos.com/agenda/\";s:30:\"config_area_Empresarial_titulo\";s:11:\"Empresarial\";s:29:\"config_area_Empresarial_texto\";s:310:\"<strong>Tenha a asseria jurídica ideal para o seu negócio</strong>\r\n<p>\r\nCom a Juriflex sua empresa recebe toda o suporte em relação aos diretiso e deveres juntos á Justiça e o Estado. A Neubert\r\nAdvogados atende empresas de diferentes portes, com competência sempre em busca das melhores decisões.</p>\";s:28:\"config_area_Empresarial_link\";s:54:\"http://neubertadvogados.hcdesenvolvimentos.com/agenda/\";s:26:\"config_area_contato_titulo\";s:13:\"FALE CONOSCO!\";s:25:\"config_area_contato_texto\";s:67:\"Preencha o formulário com seus dados e necessidade de atendimento.\";s:24:\"config_area_contato_desc\";s:33:\"Nossa equipe entrará em contato.\";s:24:\"opt_titulo_pagina_agenda\";s:22:\"Faça seu agendamento!\";s:22:\"opt_desc_pagina_agenda\";s:108:\"<p>Oferecemos assessoria jurídica <strong>Empresarial, Previdenciário, Trabalhista ou Cível</strong>.</p>\";s:26:\"opt_titulo_seu_atendimento\";s:40:\"Escolha <strong>seu atendimento</strong>\";s:19:\"opt_imagem_juriflex\";a:9:{s:3:\"url\";s:81:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/ag1.png\";s:2:\"id\";s:2:\"21\";s:6:\"height\";s:3:\"133\";s:5:\"width\";s:3:\"133\";s:9:\"thumbnail\";s:81:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/ag1.png\";s:5:\"title\";s:3:\"ag1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"opt_imagem_orientacao_avulsa\";a:9:{s:3:\"url\";s:81:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/ag2.png\";s:2:\"id\";s:2:\"22\";s:6:\"height\";s:3:\"133\";s:5:\"width\";s:3:\"133\";s:9:\"thumbnail\";s:81:\"http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/ag2.png\";s:5:\"title\";s:3:\"ag2\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:19:\"opt_titulo_juriflex\";s:8:\"Juriflex\";s:28:\"opt_titulo_orientacao_avulsa\";s:18:\"Consulta<br>Avulsa\";s:31:\"opt_titulo_agendamento_juriflex\";s:16:\"Cliente juriflex\";s:29:\"opt_desc_agendamento_juriflex\";s:0:\"\";s:21:\"opt_tempo_agendamento\";s:0:\"\";s:33:\"opt_agendamento_consulta_gratuita\";s:0:\"\";s:30:\"opt_agendamento_vamos_ate_voce\";s:0:\"\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(170, 'configuracao-transients', 'a:4:{s:14:\"changed_values\";a:1:{s:28:\"opt_titulo_orientacao_avulsa\";s:22:\"Orientação<br>Avulsa\";}s:9:\"last_save\";i:1562779957;s:13:\"last_compiler\";i:1559337319;s:11:\"last_import\";i:1559337319;}', 'yes'),
(171, 'category_children', 'a:0:{}', 'yes'),
(185, 'recovery_mode_email_last_sent', '1559339112', 'yes'),
(184, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes');

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_postmeta`
--

CREATE TABLE `nb_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_postmeta`
--

INSERT INTO `nb_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(3, 5, '_form', '<div class=\"col-sm-6\"><div class=\"areaForm\">[text* nome id:nome class:nome placeholder \"nome\"][tel* telefone id:telefone class:telefone placeholder \"telefone\"][email* email id:email class:email placeholder \"email\"]</div></div><div class=\"col-sm-6\"><div class=\"areaForm\">[select* assunto id:assunto class:assunto \"assunto\" \"previdenciário\" \"trabalhista\" \"empresarial\" \"contratos\" \"inventário\" \"cobrança\" \"tributos\" \"outros\"][textarea mensagem id:mensagem class:mensagem placeholder \"mensagem\"][submit id:enviar class:enviar \"Enviar\"]</div></div>'),
(4, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:25:\"Contado Neubert Advogados\";s:6:\"sender\";s:51:\"Neubert Advogados <contato@neubertadvogados.adv.br>\";s:9:\"recipient\";s:31:\"contato@neubertadvogados.adv.br\";s:4:\"body\";s:226:\"De: [nome]\nEmail: <[email]>\nAssunto: [assunto]\nTelefone: [telefone]\n\nCorpo da mensagem:\n[mensagem]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:34:\"Neubert Advogados \"[your-subject]\"\";s:6:\"sender\";s:69:\"Neubert Advogados <wordpress@neubertadvogados.hcdesenvolvimentos.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:161:\"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:63:\"Um ou mais campos possuem um erro. Verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:24:\"O campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato de data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:44:\"A data é posterior à maior data permitida.\";s:13:\"upload_failed\";s:49:\"Ocorreu um erro desconhecido ao enviar o arquivo.\";s:24:\"upload_file_type_invalid\";s:59:\"Você não tem permissão para enviar esse tipo de arquivo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:36:\"Ocorreu um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:46:\"O número é menor do que o mínimo permitido.\";s:16:\"number_too_large\";s:46:\"O número é maior do que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:39:\"A resposta para o quiz está incorreta.\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:45:\"O endereço de e-mail informado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválida.\";s:11:\"invalid_tel\";s:35:\"O número de telefone é inválido.\";}'),
(7, 5, '_additional_settings', ''),
(8, 5, '_locale', 'pt_BR'),
(13, 8, '_edit_lock', '1559745852:1'),
(14, 8, '_wp_page_template', 'paginas/inicial.php'),
(15, 10, '_menu_item_type', 'custom'),
(16, 10, '_menu_item_menu_item_parent', '0'),
(17, 10, '_menu_item_object_id', '10'),
(18, 10, '_menu_item_object', 'custom'),
(19, 10, '_menu_item_target', ''),
(20, 10, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(21, 10, '_menu_item_xfn', ''),
(22, 10, '_menu_item_url', '#previdenciario'),
(24, 11, '_menu_item_type', 'custom'),
(25, 11, '_menu_item_menu_item_parent', '0'),
(26, 11, '_menu_item_object_id', '11'),
(27, 11, '_menu_item_object', 'custom'),
(28, 11, '_menu_item_target', ''),
(29, 11, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(30, 11, '_menu_item_xfn', ''),
(31, 11, '_menu_item_url', '#servicos'),
(43, 14, '_edit_lock', '1562079109:1'),
(33, 12, '_menu_item_type', 'custom'),
(34, 12, '_menu_item_menu_item_parent', '0'),
(35, 12, '_menu_item_object_id', '12'),
(36, 12, '_menu_item_object', 'custom'),
(37, 12, '_menu_item_target', ''),
(38, 12, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(39, 12, '_menu_item_xfn', ''),
(40, 12, '_menu_item_url', '#juriflex'),
(42, 14, '_edit_last', '1'),
(299, 92, '_form', '[text* nome id:nome class:nome placeholder \"nome\"][email* email id:email-agendamento class:email-agendamento placeholder \"email\"][text* telefone id:telefone-agendamento class:telefone placeholder \"telefone\"][select* horario \"Horário\" \"14:00\" \"15:00\" \"16:00\" \"17:00\"][text* data id:data-atendimento-orientacao-avulsa class:hidden][text* servico id:input-servico-agenda-orientacao-avulsa class:hidden][submit \"Agendar\"]'),
(50, 17, '_wp_attached_file', '2019/05/1.png'),
(51, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:71;s:6:\"height\";i:74;s:4:\"file\";s:13:\"2019/05/1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(52, 18, '_wp_attached_file', '2019/05/2.png'),
(53, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:71;s:6:\"height\";i:74;s:4:\"file\";s:13:\"2019/05/2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(54, 19, '_wp_attached_file', '2019/05/3.png'),
(55, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:71;s:6:\"height\";i:74;s:4:\"file\";s:13:\"2019/05/3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(56, 20, '_wp_attached_file', '2019/05/4.png'),
(57, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:71;s:6:\"height\";i:74;s:4:\"file\";s:13:\"2019/05/4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(58, 21, '_wp_attached_file', '2019/05/ag1.png'),
(59, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:133;s:6:\"height\";i:133;s:4:\"file\";s:15:\"2019/05/ag1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(60, 22, '_wp_attached_file', '2019/05/ag2.png'),
(61, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:133;s:6:\"height\";i:133;s:4:\"file\";s:15:\"2019/05/ag2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(62, 23, '_edit_lock', '1559338482:1'),
(63, 24, '_wp_attached_file', '2019/05/banner.png'),
(64, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1007;s:4:\"file\";s:18:\"2019/05/banner.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"banner-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x403.png\";s:5:\"width\";i:768;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x537.png\";s:5:\"width\";i:1024;s:6:\"height\";i:537;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(65, 25, '_wp_attached_file', '2019/05/banner2.png'),
(66, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1008;s:4:\"file\";s:19:\"2019/05/banner2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner2-300x158.png\";s:5:\"width\";i:300;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner2-768x403.png\";s:5:\"width\";i:768;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner2-1024x538.png\";s:5:\"width\";i:1024;s:6:\"height\";i:538;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(67, 26, '_wp_attached_file', '2019/05/fundoServico.png'),
(68, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1247;s:4:\"file\";s:24:\"2019/05/fundoServico.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"fundoServico-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"fundoServico-300x195.png\";s:5:\"width\";i:300;s:6:\"height\";i:195;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"fundoServico-768x499.png\";s:5:\"width\";i:768;s:6:\"height\";i:499;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"fundoServico-1024x665.png\";s:5:\"width\";i:1024;s:6:\"height\";i:665;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(69, 27, '_wp_attached_file', '2019/05/q2.png'),
(70, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:66;s:4:\"file\";s:14:\"2019/05/q2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(71, 28, '_wp_attached_file', '2019/05/q3.png'),
(72, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:66;s:4:\"file\";s:14:\"2019/05/q3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(263, 77, '_wp_attached_file', '2019/06/servico5.png'),
(264, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:473;s:6:\"height\";i:508;s:4:\"file\";s:20:\"2019/06/servico5.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"servico5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"servico5-279x300.png\";s:5:\"width\";i:279;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(265, 78, '_wp_attached_file', '2019/06/servico6.png'),
(266, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:478;s:6:\"height\";i:508;s:4:\"file\";s:20:\"2019/06/servico6.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"servico6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"servico6-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(267, 79, '_wp_attached_file', '2019/06/servico7.png'),
(268, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:472;s:6:\"height\";i:508;s:4:\"file\";s:20:\"2019/06/servico7.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"servico7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"servico7-279x300.png\";s:5:\"width\";i:279;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(269, 80, '_wp_attached_file', '2019/06/servico8.png'),
(270, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:481;s:6:\"height\";i:510;s:4:\"file\";s:20:\"2019/06/servico8.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"servico8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"servico8-283x300.png\";s:5:\"width\";i:283;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(82, 33, '_edit_last', '1'),
(83, 33, '_edit_lock', '1562079880:1'),
(85, 34, '_edit_last', '1'),
(86, 34, '_edit_lock', '1562079880:1'),
(271, 81, '_wp_attached_file', '2019/06/servico9.png'),
(88, 35, '_edit_last', '1'),
(89, 35, '_edit_lock', '1562079881:1'),
(91, 23, '_wp_page_template', 'paginas/quem-somos.php'),
(92, 37, '_edit_last', '1'),
(93, 37, '_edit_lock', '1562081842:1'),
(94, 38, '_edit_lock', '1559338607:1'),
(95, 37, '_thumbnail_id', '24'),
(96, 37, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(97, 38, '_wp_page_template', 'paginas/agenda.php'),
(98, 39, '_edit_last', '1'),
(99, 39, '_edit_lock', '1562767776:1'),
(100, 39, '_thumbnail_id', '25'),
(101, 39, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(272, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:510;s:4:\"file\";s:20:\"2019/06/servico9.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"servico9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"servico9-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(273, 82, '_wp_attached_file', '2019/06/servico10.png'),
(274, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:510;s:4:\"file\";s:21:\"2019/06/servico10.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"servico10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"servico10-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(104, 42, '_wp_attached_file', '2019/05/q1.png'),
(105, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:66;s:4:\"file\";s:14:\"2019/05/q1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(107, 43, '_edit_last', '1'),
(108, 43, '_edit_lock', '1559339771:1'),
(109, 44, '_wp_attached_file', '2019/05/v1.png'),
(110, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:127;s:6:\"height\";i:99;s:4:\"file\";s:14:\"2019/05/v1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(111, 45, '_wp_attached_file', '2019/05/v2.png'),
(112, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:127;s:6:\"height\";i:99;s:4:\"file\";s:14:\"2019/05/v2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(113, 46, '_wp_attached_file', '2019/05/v3.png'),
(114, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:127;s:6:\"height\";i:99;s:4:\"file\";s:14:\"2019/05/v3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(115, 47, '_wp_attached_file', '2019/05/e3.png'),
(116, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:190;s:4:\"file\";s:14:\"2019/05/e3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"e3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(117, 48, '_wp_attached_file', '2019/05/e1.png'),
(118, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:190;s:4:\"file\";s:14:\"2019/05/e1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"e1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(119, 49, '_wp_attached_file', '2019/05/e2.png'),
(120, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:190;s:6:\"height\";i:190;s:4:\"file\";s:14:\"2019/05/e2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"e2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(121, 43, '_thumbnail_id', '49'),
(122, 50, '_edit_last', '1'),
(123, 50, '_edit_lock', '1559339791:1'),
(124, 50, '_thumbnail_id', '48'),
(125, 51, '_edit_last', '1'),
(126, 51, '_edit_lock', '1559339824:1'),
(127, 51, '_thumbnail_id', '47'),
(128, 52, '_form', '[text* nome id:nome class:nome placeholder \"nome\"][email* email id:email-agendamento class:email-agendamento placeholder \"email\"][text* telefone id:telefone-agendamento class:telefone placeholder \"telefone\"][select* horario \"Horário\" \"14:00\" \"15:00\" \"16:00\" \"17:00\"][text* data id:data-atendimento-juriflex class:hidden][text* servico id:input-servico-agenda-juriflex class:hidden][submit \"Agendar\"]'),
(129, 52, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:41:\"Agendamento Neubert Advogados - [servico]\";s:6:\"sender\";s:51:\"Neubert Advogados <contato@neubertadvogados.adv.br>\";s:9:\"recipient\";s:31:\"contato@neubertadvogados.adv.br\";s:4:\"body\";s:243:\"De: [nome]\nE-mail: <[email]>\nTelefone: [telefone]\n\nData: [data]\nHorário: [horario]\n\n\nServiço escolhido:\n[servico]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(130, 52, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:34:\"Neubert Advogados \"[your-subject]\"\";s:6:\"sender\";s:69:\"Neubert Advogados <wordpress@neubertadvogados.hcdesenvolvimentos.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:161:\"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(131, 52, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:63:\"Um ou mais campos possuem um erro. Verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:24:\"O campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato de data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:44:\"A data é posterior à maior data permitida.\";s:13:\"upload_failed\";s:49:\"Ocorreu um erro desconhecido ao enviar o arquivo.\";s:24:\"upload_file_type_invalid\";s:59:\"Você não tem permissão para enviar esse tipo de arquivo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:36:\"Ocorreu um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:46:\"O número é menor do que o mínimo permitido.\";s:16:\"number_too_large\";s:46:\"O número é maior do que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:39:\"A resposta para o quiz está incorreta.\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:45:\"O endereço de e-mail informado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválida.\";s:11:\"invalid_tel\";s:35:\"O número de telefone é inválido.\";}'),
(132, 52, '_additional_settings', ''),
(133, 52, '_locale', 'pt_BR'),
(134, 53, '_menu_item_type', 'post_type'),
(135, 53, '_menu_item_menu_item_parent', '0'),
(136, 53, '_menu_item_object_id', '38'),
(137, 53, '_menu_item_object', 'page'),
(138, 53, '_menu_item_target', ''),
(139, 53, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(140, 53, '_menu_item_xfn', ''),
(141, 53, '_menu_item_url', ''),
(161, 56, '_wp_attached_file', '2019/05/logoMarca.png'),
(143, 54, '_menu_item_type', 'post_type'),
(144, 54, '_menu_item_menu_item_parent', '0'),
(145, 54, '_menu_item_object_id', '23'),
(146, 54, '_menu_item_object', 'page'),
(147, 54, '_menu_item_target', ''),
(148, 54, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(149, 54, '_menu_item_xfn', ''),
(150, 54, '_menu_item_url', ''),
(152, 55, '_menu_item_type', 'post_type'),
(153, 55, '_menu_item_menu_item_parent', '0'),
(154, 55, '_menu_item_object_id', '8'),
(155, 55, '_menu_item_object', 'page'),
(156, 55, '_menu_item_target', ''),
(157, 55, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(158, 55, '_menu_item_xfn', ''),
(159, 55, '_menu_item_url', ''),
(160, 55, '_menu_item_orphaned', '1559340509'),
(162, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:162;s:6:\"height\";i:114;s:4:\"file\";s:21:\"2019/05/logoMarca.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"logoMarca-150x114.png\";s:5:\"width\";i:150;s:6:\"height\";i:114;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(163, 57, '_wp_attached_file', '2019/05/logoRodape.png'),
(164, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:199;s:6:\"height\";i:139;s:4:\"file\";s:22:\"2019/05/logoRodape.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"logoRodape-150x139.png\";s:5:\"width\";i:150;s:6:\"height\";i:139;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(165, 58, '_menu_item_type', 'custom'),
(166, 58, '_menu_item_menu_item_parent', '0'),
(167, 58, '_menu_item_object_id', '58'),
(168, 58, '_menu_item_object', 'custom'),
(169, 58, '_menu_item_target', ''),
(170, 58, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(171, 58, '_menu_item_xfn', ''),
(172, 58, '_menu_item_url', '#contato'),
(174, 59, '_menu_item_type', 'post_type'),
(175, 59, '_menu_item_menu_item_parent', '0'),
(176, 59, '_menu_item_object_id', '38'),
(177, 59, '_menu_item_object', 'page'),
(178, 59, '_menu_item_target', ''),
(179, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(180, 59, '_menu_item_xfn', ''),
(181, 59, '_menu_item_url', ''),
(183, 60, '_menu_item_type', 'post_type'),
(184, 60, '_menu_item_menu_item_parent', '0'),
(185, 60, '_menu_item_object_id', '23'),
(186, 60, '_menu_item_object', 'page'),
(187, 60, '_menu_item_target', ''),
(188, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(189, 60, '_menu_item_xfn', ''),
(190, 60, '_menu_item_url', ''),
(192, 61, '_menu_item_type', 'custom'),
(193, 61, '_menu_item_menu_item_parent', '0'),
(194, 61, '_menu_item_object_id', '61'),
(195, 61, '_menu_item_object', 'custom'),
(196, 61, '_menu_item_target', ''),
(197, 61, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(198, 61, '_menu_item_xfn', ''),
(199, 61, '_menu_item_url', '#contato'),
(201, 62, '_menu_item_type', 'custom'),
(202, 62, '_menu_item_menu_item_parent', '0'),
(203, 62, '_menu_item_object_id', '62'),
(204, 62, '_menu_item_object', 'custom'),
(205, 62, '_menu_item_target', ''),
(206, 62, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(207, 62, '_menu_item_xfn', ''),
(208, 62, '_menu_item_url', 'http://neubertadvogados.hcdesenvolvimentos.com/#servicos'),
(210, 63, '_menu_item_type', 'custom'),
(211, 63, '_menu_item_menu_item_parent', '0'),
(212, 63, '_menu_item_object_id', '63'),
(213, 63, '_menu_item_object', 'custom'),
(214, 63, '_menu_item_target', ''),
(215, 63, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(216, 63, '_menu_item_xfn', ''),
(217, 63, '_menu_item_url', 'http://neubertadvogados.hcdesenvolvimentos.com/#previdenciario'),
(219, 64, '_menu_item_type', 'custom'),
(220, 64, '_menu_item_menu_item_parent', '0'),
(221, 64, '_menu_item_object_id', '64'),
(222, 64, '_menu_item_object', 'custom'),
(223, 64, '_menu_item_target', ''),
(224, 64, '_menu_item_classes', 'a:1:{i:0;s:13:\"scrollTopLink\";}'),
(225, 64, '_menu_item_xfn', ''),
(226, 64, '_menu_item_url', 'http://neubertadvogados.hcdesenvolvimentos.com/#juriflex'),
(228, 66, '_edit_last', '1'),
(229, 66, '_edit_lock', '1562768003:1'),
(230, 67, '_wp_attached_file', '2019/06/banner3.png'),
(231, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1938;s:6:\"height\";i:1001;s:4:\"file\";s:19:\"2019/06/banner3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner3-300x155.png\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner3-768x397.png\";s:5:\"width\";i:768;s:6:\"height\";i:397;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner3-1024x529.png\";s:5:\"width\";i:1024;s:6:\"height\";i:529;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(232, 68, '_wp_attached_file', '2019/06/banner4.png'),
(233, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1988;s:6:\"height\";i:1021;s:4:\"file\";s:19:\"2019/06/banner4.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner4-300x154.png\";s:5:\"width\";i:300;s:6:\"height\";i:154;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner4-768x394.png\";s:5:\"width\";i:768;s:6:\"height\";i:394;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner4-1024x526.png\";s:5:\"width\";i:1024;s:6:\"height\";i:526;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(234, 69, '_wp_attached_file', '2019/06/banner5.png'),
(235, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1959;s:6:\"height\";i:1027;s:4:\"file\";s:19:\"2019/06/banner5.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner5-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner5-768x403.png\";s:5:\"width\";i:768;s:6:\"height\";i:403;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner5-1024x537.png\";s:5:\"width\";i:1024;s:6:\"height\";i:537;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(236, 70, '_wp_attached_file', '2019/06/banner6.png'),
(237, 70, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1919;s:6:\"height\";i:1032;s:4:\"file\";s:19:\"2019/06/banner6.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner6-300x161.png\";s:5:\"width\";i:300;s:6:\"height\";i:161;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner6-768x413.png\";s:5:\"width\";i:768;s:6:\"height\";i:413;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner6-1024x551.png\";s:5:\"width\";i:1024;s:6:\"height\";i:551;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(238, 71, '_wp_attached_file', '2019/06/banner7.png'),
(239, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1955;s:6:\"height\";i:1031;s:4:\"file\";s:19:\"2019/06/banner7.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"banner7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"banner7-300x158.png\";s:5:\"width\";i:300;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"banner7-768x405.png\";s:5:\"width\";i:768;s:6:\"height\";i:405;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"banner7-1024x540.png\";s:5:\"width\";i:1024;s:6:\"height\";i:540;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(240, 66, '_thumbnail_id', '71'),
(241, 66, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(242, 66, 'NubertAdvogados_destaque_checkbox', '1'),
(243, 72, '_edit_last', '1'),
(244, 72, '_edit_lock', '1562768004:1'),
(245, 72, '_thumbnail_id', '70'),
(246, 72, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(247, 72, 'NubertAdvogados_destaque_checkbox', '0'),
(248, 73, '_edit_last', '1'),
(249, 73, '_edit_lock', '1562081841:1'),
(250, 73, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(251, 73, 'NubertAdvogados_destaque_checkbox', '0'),
(252, 74, '_edit_last', '1'),
(253, 74, '_edit_lock', '1562081841:1'),
(254, 74, '_thumbnail_id', '68'),
(255, 74, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(256, 74, 'NubertAdvogados_destaque_checkbox', '1'),
(257, 75, '_edit_last', '1'),
(258, 75, '_edit_lock', '1562768003:1'),
(259, 75, '_thumbnail_id', '67'),
(260, 75, 'NubertAdvogados_destaque_link', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(261, 75, 'NubertAdvogados_destaque_checkbox', '0'),
(262, 73, '_thumbnail_id', '69'),
(275, 83, '_wp_attached_file', '2019/06/servico11.png'),
(276, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:510;s:4:\"file\";s:21:\"2019/06/servico11.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"servico11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"servico11-282x300.png\";s:5:\"width\";i:282;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(277, 35, '_thumbnail_id', '80'),
(278, 34, '_thumbnail_id', '81'),
(279, 33, '_thumbnail_id', '82'),
(280, 14, '_thumbnail_id', '83'),
(281, 84, '_edit_last', '1'),
(282, 84, '_edit_lock', '1562079881:1'),
(283, 84, '_thumbnail_id', '79'),
(284, 85, '_edit_last', '1'),
(285, 85, '_edit_lock', '1562079881:1'),
(286, 85, '_thumbnail_id', '78'),
(287, 86, '_edit_last', '1'),
(288, 86, '_edit_lock', '1562079881:1'),
(289, 86, '_thumbnail_id', '77'),
(290, 88, '_wp_attached_file', '2019/06/fotoPrevidenciario.png'),
(291, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:866;s:6:\"height\";i:715;s:4:\"file\";s:30:\"2019/06/fotoPrevidenciario.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"fotoPrevidenciario-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"fotoPrevidenciario-300x248.png\";s:5:\"width\";i:300;s:6:\"height\";i:248;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"fotoPrevidenciario-768x634.png\";s:5:\"width\";i:768;s:6:\"height\";i:634;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(292, 86, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(293, 85, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(294, 84, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(295, 35, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(296, 34, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(297, 33, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(298, 14, 'NubertAdvogados_servico_link_botao', 'http://neubertadvogados.hcdesenvolvimentos.com/agenda/'),
(300, 92, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:41:\"Agendamento Neubert Advogados - [servico]\";s:6:\"sender\";s:51:\"Neubert Advogados <contato@neubertadvogados.adv.br>\";s:9:\"recipient\";s:31:\"contato@neubertadvogados.adv.br\";s:4:\"body\";s:243:\"De: [nome]\nE-mail: <[email]>\nTelefone: [telefone]\n\nData: [data]\nHorário: [horario]\n\n\nServiço escolhido:\n[servico]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(301, 92, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:34:\"Neubert Advogados \"[your-subject]\"\";s:6:\"sender\";s:69:\"Neubert Advogados <wordpress@neubertadvogados.hcdesenvolvimentos.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:161:\"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(302, 92, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:63:\"Um ou mais campos possuem um erro. Verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:24:\"O campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato de data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:44:\"A data é posterior à maior data permitida.\";s:13:\"upload_failed\";s:49:\"Ocorreu um erro desconhecido ao enviar o arquivo.\";s:24:\"upload_file_type_invalid\";s:59:\"Você não tem permissão para enviar esse tipo de arquivo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:36:\"Ocorreu um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:46:\"O número é menor do que o mínimo permitido.\";s:16:\"number_too_large\";s:46:\"O número é maior do que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:39:\"A resposta para o quiz está incorreta.\";s:17:\"captcha_not_match\";s:35:\"O código digitado está incorreto.\";s:13:\"invalid_email\";s:45:\"O endereço de e-mail informado é inválido.\";s:11:\"invalid_url\";s:19:\"A URL é inválida.\";s:11:\"invalid_tel\";s:35:\"O número de telefone é inválido.\";}'),
(303, 92, '_additional_settings', ''),
(304, 92, '_locale', 'pt_BR'),
(306, 93, '_wp_attached_file', '2019/07/logo.png'),
(307, 93, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:162;s:6:\"height\";i:114;s:4:\"file\";s:16:\"2019/07/logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x114.png\";s:5:\"width\";i:150;s:6:\"height\";i:114;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(308, 39, 'NubertAdvogados_destaque_checkbox', '0'),
(309, 37, 'NubertAdvogados_destaque_checkbox', '0'),
(310, 39, '_wp_old_slug', 'tenha-a-assessoria-juridica'),
(311, 37, '_wp_old_slug', 'solicite-sua-aposentadoria-com-transparencia'),
(312, 66, '_wp_old_slug', 'aporte-juridico-na-aplicacao-das-leis-e-processos-na-justica-do-trabalho'),
(313, 72, '_wp_old_slug', 'assessoria-integra-em-demandas-judiciais'),
(314, 73, '_wp_old_slug', 'receba-suporte-em-questoes-de-direito-medico-e-hospitalar'),
(315, 74, '_wp_old_slug', 'orientacao-em-procedimentos-fiscais-e-recuperacao-de-credito-tributario'),
(316, 75, '_wp_old_slug', 'assessoria-juridica-em-testamentos-inventarios-e-escrituras-publicas'),
(324, 92, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}'),
(325, 52, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}'),
(326, 5, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}');

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_posts`
--

CREATE TABLE `nb_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_posts`
--

INSERT INTO `nb_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-31 18:08:56', '2019-05-31 21:08:56', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-05-31 18:08:56', '2019-05-31 21:08:56', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=1', 0, 'post', '', 1),
(66, 1, '2019-06-12 19:10:50', '2019-06-12 22:10:50', 'Aporte jurídico na aplicação das\r\n<strong>leis e processos na<br>justiça do trabalho</strong>', 'Trabalhista', '', 'publish', 'closed', 'closed', '', 'trabalhista', '', '', '2019-07-10 11:13:41', '2019-07-10 14:13:41', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=66', 2, 'destaque', '', 0),
(5, 1, '2019-05-31 18:11:26', '2019-05-31 21:11:26', '<div class=\"col-sm-6\"><div class=\"areaForm\">[text* nome id:nome class:nome placeholder \"nome\"][tel* telefone id:telefone class:telefone placeholder \"telefone\"][email* email id:email class:email placeholder \"email\"]</div></div><div class=\"col-sm-6\"><div class=\"areaForm\">[select* assunto id:assunto class:assunto \"assunto\" \"previdenciário\" \"trabalhista\" \"empresarial\" \"contratos\" \"inventário\" \"cobrança\" \"tributos\" \"outros\"][textarea mensagem id:mensagem class:mensagem placeholder \"mensagem\"][submit id:enviar class:enviar \"Enviar\"]</div></div>\n1\nContado Neubert Advogados\nNeubert Advogados <contato@neubertadvogados.adv.br>\ncontato@neubertadvogados.adv.br\nDe: [nome]\r\nEmail: <[email]>\r\nAssunto: [assunto]\r\nTelefone: [telefone]\r\n\r\nCorpo da mensagem:\r\n[mensagem]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\n\n\n\n\n\nNeubert Advogados \"[your-subject]\"\nNeubert Advogados <wordpress@neubertadvogados.hcdesenvolvimentos.com>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Formulário de contato', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato-1', '', '', '2019-07-10 12:19:28', '2019-07-10 15:19:28', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(8, 1, '2019-05-31 18:16:01', '2019-05-31 21:16:01', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-05-31 18:16:01', '2019-05-31 21:16:01', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-05-31 18:16:01', '2019-05-31 21:16:01', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-05-31 18:16:01', '2019-05-31 21:16:01', '', 8, 'http://neubertadvogados.hcdesenvolvimentos.com/2019/05/31/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2019-05-31 18:29:17', '2019-05-31 21:29:17', '', 'Previdenciário', '', 'publish', 'closed', 'closed', '', 'previdenciario', '', '', '2019-06-04 10:14:00', '2019-06-04 13:14:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=10', 2, 'nav_menu_item', '', 0),
(11, 1, '2019-05-31 18:29:17', '2019-05-31 21:29:17', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2019-06-04 10:14:00', '2019-06-04 13:14:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=11', 4, 'nav_menu_item', '', 0),
(12, 1, '2019-05-31 18:29:17', '2019-05-31 21:29:17', '', 'Juriflex', '', 'publish', 'closed', 'closed', '', 'juriflex', '', '', '2019-06-04 10:14:00', '2019-06-04 13:14:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=12', 3, 'nav_menu_item', '', 0),
(96, 1, '2019-07-10 11:07:18', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-07-10 11:07:18', '0000-00-00 00:00:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=96', 0, 'post', '', 0),
(14, 1, '2019-05-31 18:32:03', '2019-05-31 21:32:03', '<strong>Tenha a assessoria jurídica ideal para o seu negócio.</strong>\r\n<p>Com a Juriflex sua empresa recebe toda o suporte em relação aos direitos e deveres junto à Justiça e o Estado. A Neubert Advogados atende empresas de diferentes portes, com competência sempre em busca das melhores decisões.</p>', 'Empresarial', '', 'publish', 'closed', 'closed', '', 'empresarial', '', '', '2019-07-02 11:54:10', '2019-07-02 14:54:10', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=14', 0, 'servico', '', 0),
(92, 1, '2019-07-01 12:30:35', '2019-07-01 15:30:35', '[text* nome id:nome class:nome placeholder \"nome\"][email* email id:email-agendamento class:email-agendamento placeholder \"email\"][text* telefone id:telefone-agendamento class:telefone placeholder \"telefone\"][select* horario \"Horário\" \"14:00\" \"15:00\" \"16:00\" \"17:00\"][text* data id:data-atendimento-orientacao-avulsa class:hidden][text* servico id:input-servico-agenda-orientacao-avulsa class:hidden][submit \"Agendar\"]\n1\nAgendamento Neubert Advogados - [servico]\nNeubert Advogados <contato@neubertadvogados.adv.br>\ncontato@neubertadvogados.adv.br\nDe: [nome]\r\nE-mail: <[email]>\r\nTelefone: [telefone]\r\n\r\nData: [data]\r\nHorário: [horario]\r\n\r\n\r\nServiço escolhido:\r\n[servico]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\n\n\n\n\n\nNeubert Advogados \"[your-subject]\"\nNeubert Advogados <wordpress@neubertadvogados.hcdesenvolvimentos.com>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Fomulário de agendamento(orientação avulsa)', '', 'publish', 'closed', 'closed', '', 'fomulario-de-agendamentoorientacao-avulsa', '', '', '2019-07-10 12:17:48', '2019-07-10 15:17:48', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=wpcf7_contact_form&#038;p=92', 0, 'wpcf7_contact_form', '', 0),
(93, 1, '2019-07-01 16:33:40', '2019-07-01 19:33:40', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2019-07-01 16:33:40', '2019-07-01 19:33:40', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/07/logo.png', 0, 'attachment', 'image/png', 0),
(94, 1, '2019-07-02 11:53:11', '2019-07-02 14:53:11', '<strong>Tenha a assessoria jurídica ideal para o seu negócio.</strong>\n<p>Com a Juriflex sua empresa recebe toda o suporte em relação aos diretiso e deveres juntos á Justiça e o Estado. A Neubert Advogados atende empresas de diferentes portes, com competência sempre em busca das melhores decisões.</p>', 'Empresarial', '', 'inherit', 'closed', 'closed', '', '14-autosave-v1', '', '', '2019-07-02 11:53:11', '2019-07-02 14:53:11', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/2019/07/02/14-autosave-v1/', 0, 'revision', '', 0),
(17, 1, '2019-05-31 18:31:26', '2019-05-31 21:31:26', '', '1', '', 'inherit', 'open', 'closed', '', '1', '', '', '2019-05-31 18:31:26', '2019-05-31 21:31:26', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/1.png', 0, 'attachment', 'image/png', 0),
(18, 1, '2019-05-31 18:31:27', '2019-05-31 21:31:27', '', '2', '', 'inherit', 'open', 'closed', '', '2', '', '', '2019-05-31 18:31:27', '2019-05-31 21:31:27', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/2.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2019-05-31 18:31:28', '2019-05-31 21:31:28', '', '3', '', 'inherit', 'open', 'closed', '', '3', '', '', '2019-05-31 18:31:28', '2019-05-31 21:31:28', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/3.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2019-05-31 18:31:29', '2019-05-31 21:31:29', '', '4', '', 'inherit', 'open', 'closed', '', '4', '', '', '2019-05-31 18:31:29', '2019-05-31 21:31:29', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/4.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2019-05-31 18:31:30', '2019-05-31 21:31:30', '', 'ag1', '', 'inherit', 'open', 'closed', '', 'ag1', '', '', '2019-05-31 18:31:30', '2019-05-31 21:31:30', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/ag1.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2019-05-31 18:31:31', '2019-05-31 21:31:31', '', 'ag2', '', 'inherit', 'open', 'closed', '', 'ag2', '', '', '2019-05-31 18:31:31', '2019-05-31 21:31:31', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/ag2.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2019-05-31 18:33:36', '2019-05-31 21:33:36', '', 'Quem somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2019-05-31 18:33:45', '2019-05-31 21:33:45', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?page_id=23', 0, 'page', '', 0),
(24, 1, '2019-05-31 18:31:36', '2019-05-31 21:31:36', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2019-05-31 18:31:36', '2019-05-31 21:31:36', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/banner.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2019-05-31 18:31:42', '2019-05-31 21:31:42', '', 'banner2', '', 'inherit', 'open', 'closed', '', 'banner2', '', '', '2019-05-31 18:31:42', '2019-05-31 21:31:42', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/banner2.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2019-05-31 18:31:45', '2019-05-31 21:31:45', '', 'fundoServico', '', 'inherit', 'open', 'closed', '', 'fundoservico', '', '', '2019-05-31 18:31:45', '2019-05-31 21:31:45', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/fundoServico.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2019-05-31 18:31:47', '2019-05-31 21:31:47', '', 'q2', '', 'inherit', 'open', 'closed', '', 'q2', '', '', '2019-05-31 18:31:47', '2019-05-31 21:31:47', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q2.png', 0, 'attachment', 'image/png', 0),
(28, 1, '2019-05-31 18:31:48', '2019-05-31 21:31:48', '', 'q3', '', 'inherit', 'open', 'closed', '', 'q3', '', '', '2019-05-31 18:31:48', '2019-05-31 21:31:48', '', 14, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q3.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2019-06-13 13:29:12', '2019-06-13 16:29:12', '', 'servico5', '', 'inherit', 'open', 'closed', '', 'servico5', '', '', '2019-06-13 13:29:12', '2019-06-13 16:29:12', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico5.png', 0, 'attachment', 'image/png', 0),
(78, 1, '2019-06-13 13:29:14', '2019-06-13 16:29:14', '', 'servico6', '', 'inherit', 'open', 'closed', '', 'servico6', '', '', '2019-06-13 13:29:14', '2019-06-13 16:29:14', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico6.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2019-06-13 13:29:15', '2019-06-13 16:29:15', '', 'servico7', '', 'inherit', 'open', 'closed', '', 'servico7', '', '', '2019-06-13 13:29:15', '2019-06-13 16:29:15', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico7.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2019-06-13 13:29:17', '2019-06-13 16:29:17', '', 'servico8', '', 'inherit', 'open', 'closed', '', 'servico8', '', '', '2019-06-13 13:29:17', '2019-06-13 16:29:17', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico8.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2019-05-31 18:32:55', '2019-05-31 21:32:55', '<strong>Solicite sua aposentadoria com transparência e segurança.</strong>\r\n<p>Nossa equipe atua no suporte e nas ações necessárias para que contribuintes da Previdência possam obter seus benefícios. Analisamos a regulamentação, examinamos cada caso e oferecemos assessoria jurídica na contagem de tempo; planejamento previdenciário; requerimento da aposentadoria ou revisão do benefício.</p>', 'Previdenciário', '', 'publish', 'closed', 'closed', '', 'previdenciario', '', '', '2019-07-02 11:55:02', '2019-07-02 14:55:02', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=33', 0, 'servico', '', 0),
(34, 1, '2019-05-31 18:33:13', '2019-05-31 21:33:13', '<strong>Aporte jurídico na aplicação das leis e processos na Justiça do Trabalho.</strong>\r\n<p>Assessoramos seu negócio para que este opere com as melhores práticas de atividade empresarial. Oferecemos consultoria e orientação preventiva na identificação de pontos cruciais que possam gerar ações judiciais e reclamatórias trabalhistas.</p>', 'Trabalhista', '', 'publish', 'closed', 'closed', '', 'trabalhista', '', '', '2019-07-02 11:55:49', '2019-07-02 14:55:49', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=34', 0, 'servico', '', 0),
(35, 1, '2019-05-31 18:33:35', '2019-05-31 21:33:35', '<strong>Assessoria integral em demandas judiciais.</strong>\r\n<p>Atuamos com eficiência em ações cíveis nos tribunais judiciais, sejam nos litígios declaratórios, rescisórios, anulatórios ou indenizatórios. Além disso, intervimos no uso de medidas cautelares e tutelas antecipatórias.</p>', 'Cível', '', 'publish', 'closed', 'closed', '', 'civel', '', '', '2019-07-02 11:57:08', '2019-07-02 14:57:08', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=35', 0, 'servico', '', 0),
(36, 1, '2019-05-31 18:33:36', '2019-05-31 21:33:36', '', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2019-05-31 18:33:36', '2019-05-31 21:33:36', '', 23, 'http://neubertadvogados.hcdesenvolvimentos.com/2019/05/31/23-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2019-05-31 18:34:45', '2019-05-31 21:34:45', 'Solicite sua\r\n<strong>APOSENTADORIA\r\nCOM TRANSPARÊNCIA</strong>\r\ne segurança', 'Previdenciário', '', 'publish', 'closed', 'closed', '', 'previdenciario', '', '', '2019-07-02 12:38:05', '2019-07-02 15:38:05', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=37', 1, 'destaque', '', 0),
(38, 1, '2019-05-31 18:34:54', '2019-05-31 21:34:54', '', 'Agenda', '', 'publish', 'closed', 'closed', '', 'agenda', '', '', '2019-05-31 18:34:54', '2019-05-31 21:34:54', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?page_id=38', 0, 'page', '', 0),
(39, 1, '2019-05-31 18:35:20', '2019-05-31 21:35:20', 'Tenha a Assessória Jurídica\r\n<strong>Ideal para <br class=\"br\" />o seu negócio</strong>', 'Empresarial', '', 'publish', 'closed', 'closed', '', 'empresarial', '', '', '2019-07-02 12:37:46', '2019-07-02 15:37:46', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=39', 0, 'destaque', '', 0),
(40, 1, '2019-05-31 18:34:54', '2019-05-31 21:34:54', '', 'Agenda', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2019-05-31 18:34:54', '2019-05-31 21:34:54', '', 38, 'http://neubertadvogados.hcdesenvolvimentos.com/2019/05/31/38-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2019-06-13 13:29:19', '2019-06-13 16:29:19', '', 'servico9', '', 'inherit', 'open', 'closed', '', 'servico9', '', '', '2019-06-13 13:29:19', '2019-06-13 16:29:19', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico9.png', 0, 'attachment', 'image/png', 0),
(42, 1, '2019-05-31 18:51:27', '2019-05-31 21:51:27', '', 'q1', '', 'inherit', 'open', 'closed', '', 'q1', '', '', '2019-05-31 18:51:27', '2019-05-31 21:51:27', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q1.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2019-05-31 18:58:32', '2019-05-31 21:58:32', '', 'Alexandre Neubert', '', 'publish', 'closed', 'closed', '', 'alexandre-neubert', '', '', '2019-05-31 18:58:32', '2019-05-31 21:58:32', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=profissional&#038;p=43', 0, 'profissional', '', 0),
(44, 1, '2019-05-31 18:58:16', '2019-05-31 21:58:16', '', 'v1', '', 'inherit', 'open', 'closed', '', 'v1', '', '', '2019-05-31 18:58:16', '2019-05-31 21:58:16', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v1.png', 0, 'attachment', 'image/png', 0),
(45, 1, '2019-05-31 18:58:17', '2019-05-31 21:58:17', '', 'v2', '', 'inherit', 'open', 'closed', '', 'v2', '', '', '2019-05-31 18:58:17', '2019-05-31 21:58:17', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v2.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2019-05-31 18:58:18', '2019-05-31 21:58:18', '', 'v3', '', 'inherit', 'open', 'closed', '', 'v3', '', '', '2019-05-31 18:58:18', '2019-05-31 21:58:18', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/v3.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2019-05-31 18:58:27', '2019-05-31 21:58:27', '', 'e3', '', 'inherit', 'open', 'closed', '', 'e3', '', '', '2019-05-31 18:58:27', '2019-05-31 21:58:27', '', 43, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/e3.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2019-05-31 18:58:28', '2019-05-31 21:58:28', '', 'e1', '', 'inherit', 'open', 'closed', '', 'e1', '', '', '2019-05-31 18:58:28', '2019-05-31 21:58:28', '', 43, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/e1.png', 0, 'attachment', 'image/png', 0),
(49, 1, '2019-05-31 18:58:29', '2019-05-31 21:58:29', '', 'e2', '', 'inherit', 'open', 'closed', '', 'e2', '', '', '2019-05-31 18:58:29', '2019-05-31 21:58:29', '', 43, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/e2.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2019-05-31 18:58:52', '2019-05-31 21:58:52', '', 'Fernanda Neubert', '', 'publish', 'closed', 'closed', '', 'fernanda-neubert', '', '', '2019-05-31 18:58:52', '2019-05-31 21:58:52', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=profissional&#038;p=50', 0, 'profissional', '', 0),
(51, 1, '2019-05-31 18:59:08', '2019-05-31 21:59:08', '', 'Bárbara Neubert', '', 'publish', 'closed', 'closed', '', 'barbara-neubert', '', '', '2019-05-31 18:59:08', '2019-05-31 21:59:08', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=profissional&#038;p=51', 0, 'profissional', '', 0),
(52, 1, '2019-05-31 19:03:59', '2019-05-31 22:03:59', '[text* nome id:nome class:nome placeholder \"nome\"][email* email id:email-agendamento class:email-agendamento placeholder \"email\"][text* telefone id:telefone-agendamento class:telefone placeholder \"telefone\"][select* horario \"Horário\" \"14:00\" \"15:00\" \"16:00\" \"17:00\"][text* data id:data-atendimento-juriflex class:hidden][text* servico id:input-servico-agenda-juriflex class:hidden][submit \"Agendar\"]\n1\nAgendamento Neubert Advogados - [servico]\nNeubert Advogados <contato@neubertadvogados.adv.br>\ncontato@neubertadvogados.adv.br\nDe: [nome]\r\nE-mail: <[email]>\r\nTelefone: [telefone]\r\n\r\nData: [data]\r\nHorário: [horario]\r\n\r\n\r\nServiço escolhido:\r\n[servico]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\n\n\n\n\n\nNeubert Advogados \"[your-subject]\"\nNeubert Advogados <wordpress@neubertadvogados.hcdesenvolvimentos.com>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Neubert Advogados (http://neubertadvogados.hcdesenvolvimentos.com)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Formulário de agendamento(juriflex)', '', 'publish', 'closed', 'closed', '', 'formulario-de-agendamento', '', '', '2019-07-10 12:17:53', '2019-07-10 15:17:53', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=wpcf7_contact_form&#038;p=52', 0, 'wpcf7_contact_form', '', 0),
(53, 1, '2019-05-31 19:08:50', '2019-05-31 22:08:50', ' ', '', '', 'publish', 'closed', 'closed', '', '53', '', '', '2019-06-04 10:14:00', '2019-06-04 13:14:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=53', 5, 'nav_menu_item', '', 0),
(54, 1, '2019-05-31 19:08:50', '2019-05-31 22:08:50', ' ', '', '', 'publish', 'closed', 'closed', '', '54', '', '', '2019-06-04 10:14:00', '2019-06-04 13:14:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=54', 1, 'nav_menu_item', '', 0),
(55, 1, '2019-05-31 19:08:29', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-05-31 19:08:29', '0000-00-00 00:00:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=55', 1, 'nav_menu_item', '', 0),
(56, 1, '2019-05-31 23:19:43', '2019-06-01 02:19:43', '', 'logoMarca', '', 'inherit', 'open', 'closed', '', 'logomarca', '', '', '2019-05-31 23:19:43', '2019-06-01 02:19:43', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/logoMarca.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2019-05-31 23:20:22', '2019-06-01 02:20:22', '', 'logoRodape', '', 'inherit', 'open', 'closed', '', 'logorodape', '', '', '2019-05-31 23:20:22', '2019-06-01 02:20:22', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/05/logoRodape.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2019-06-04 10:06:53', '2019-06-04 13:06:53', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2019-06-04 10:14:00', '2019-06-04 13:14:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=58', 6, 'nav_menu_item', '', 0),
(59, 1, '2019-06-06 10:31:17', '2019-06-06 13:31:17', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2019-06-06 10:46:55', '2019-06-06 13:46:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=59', 5, 'nav_menu_item', '', 0),
(60, 1, '2019-06-06 10:31:17', '2019-06-06 13:31:17', ' ', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2019-06-06 10:46:55', '2019-06-06 13:46:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=60', 1, 'nav_menu_item', '', 0),
(61, 1, '2019-06-06 10:31:17', '2019-06-06 13:31:17', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato-2', '', '', '2019-06-06 10:46:55', '2019-06-06 13:46:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=61', 6, 'nav_menu_item', '', 0),
(62, 1, '2019-06-06 10:31:17', '2019-06-06 13:31:17', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos-2', '', '', '2019-06-06 10:46:55', '2019-06-06 13:46:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=62', 4, 'nav_menu_item', '', 0),
(63, 1, '2019-06-06 10:31:17', '2019-06-06 13:31:17', '', 'Previdenciário', '', 'publish', 'closed', 'closed', '', 'previdenciario-2', '', '', '2019-06-06 10:46:55', '2019-06-06 13:46:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=63', 2, 'nav_menu_item', '', 0),
(64, 1, '2019-06-06 10:31:17', '2019-06-06 13:31:17', '', 'Juriflex', '', 'publish', 'closed', 'closed', '', 'juriflex-2', '', '', '2019-06-06 10:46:55', '2019-06-06 13:46:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?p=64', 3, 'nav_menu_item', '', 0),
(67, 1, '2019-06-12 19:10:22', '2019-06-12 22:10:22', '', 'banner3', '', 'inherit', 'open', 'closed', '', 'banner3', '', '', '2019-06-12 19:10:22', '2019-06-12 22:10:22', '', 66, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner3.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2019-06-12 19:10:29', '2019-06-12 22:10:29', '', 'banner4', '', 'inherit', 'open', 'closed', '', 'banner4', '', '', '2019-06-12 19:10:29', '2019-06-12 22:10:29', '', 66, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner4.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2019-06-12 19:10:32', '2019-06-12 22:10:32', '', 'banner5', '', 'inherit', 'open', 'closed', '', 'banner5', '', '', '2019-06-12 19:10:32', '2019-06-12 22:10:32', '', 66, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner5.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2019-06-12 19:10:36', '2019-06-12 22:10:36', '', 'banner6', '', 'inherit', 'open', 'closed', '', 'banner6', '', '', '2019-06-12 19:10:36', '2019-06-12 22:10:36', '', 66, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner6.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2019-06-12 19:10:41', '2019-06-12 22:10:41', '', 'banner7', '', 'inherit', 'open', 'closed', '', 'banner7', '', '', '2019-06-12 19:10:41', '2019-06-12 22:10:41', '', 66, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner7.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2019-06-12 19:11:31', '2019-06-12 22:11:31', 'Assessoria integral em\r\n<strong>demandas judiciais</strong>', 'Cível', '', 'publish', 'closed', 'closed', '', 'civel', '', '', '2019-07-10 11:11:23', '2019-07-10 14:11:23', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=72', 3, 'destaque', '', 0),
(73, 1, '2019-06-12 19:12:26', '2019-06-12 22:12:26', 'Receba suporte em\r\n<strong>questões de<br>direito médico<br>e hospitalar</strong>', 'Saúde', '', 'publish', 'closed', 'closed', '', 'saude', '', '', '2019-07-02 12:38:48', '2019-07-02 15:38:48', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=73', 4, 'destaque', '', 0),
(74, 1, '2019-06-12 19:13:24', '2019-06-12 22:13:24', 'Orientação em<br>procedimentos fiscais e\r\n<strong>recuperação<br>de crédito<br>tributário</strong>', 'Tributário', '', 'publish', 'closed', 'closed', '', 'tributario', '', '', '2019-07-02 12:39:00', '2019-07-02 15:39:00', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=74', 5, 'destaque', '', 0),
(75, 1, '2019-06-12 19:14:12', '2019-06-12 22:14:12', 'Assessoria jurídica em testamentos,\r\n<strong>Inventários e escrituras<br>públicas.</strong>', 'Inventário', '', 'publish', 'closed', 'closed', '', 'inventario', '', '', '2019-07-10 11:12:17', '2019-07-10 14:12:17', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=destaque&#038;p=75', 6, 'destaque', '', 0),
(82, 1, '2019-06-13 13:29:20', '2019-06-13 16:29:20', '', 'servico10', '', 'inherit', 'open', 'closed', '', 'servico10', '', '', '2019-06-13 13:29:20', '2019-06-13 16:29:20', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico10.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2019-06-13 13:29:21', '2019-06-13 16:29:21', '', 'servico11', '', 'inherit', 'open', 'closed', '', 'servico11', '', '', '2019-06-13 13:29:21', '2019-06-13 16:29:21', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/servico11.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2019-06-13 13:31:55', '2019-06-13 16:31:55', '<strong>Suporte em questões médicas e hospitalares.</strong>\r\n<p>Fornecemos amparo no direito médico e hospitalar, atuando para a garantia do direito à saúde e equilíbrio na relação clínica.</p>', 'Saúde', '', 'publish', 'closed', 'closed', '', 'saude', '', '', '2019-07-02 11:57:32', '2019-07-02 14:57:32', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=84', 0, 'servico', '', 0),
(85, 1, '2019-06-13 13:32:19', '2019-06-13 16:32:19', '<strong>Assessoria tributária e defesa em contencioso fiscal.</strong>\r\n<p>Prestamos assessoria em procedimento fiscal; oferecemos defesa em execução fiscal; e atuamos em casos de recuperação de crédito tributário. Tanto na área administrativa quanto judicial, atuamos na defesa de objetos de contestação, disputa ou conflito de interesses.</p>', 'Tributário', '', 'publish', 'closed', 'closed', '', 'tirbutario', '', '', '2019-07-02 11:57:55', '2019-07-02 14:57:55', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=85', 0, 'servico', '', 0),
(86, 1, '2019-06-13 13:32:34', '2019-06-13 16:32:34', '<strong>Orientação jurídica em casos de sucessão e partilha.</strong>\r\n<p>Prestamos assessoria para casos a elaboração de inventários, bem como oferecemos orientação jurídica na sucessão. Atuamos na elaboração de testamentos e promoção de inventário extrajudicial e judicial e escrituras públicas.</p>', 'Inventário', '', 'publish', 'closed', 'closed', '', 'inventario', '', '', '2019-07-02 11:58:37', '2019-07-02 14:58:37', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/?post_type=servico&#038;p=86', 0, 'servico', '', 0),
(88, 1, '2019-06-13 13:43:18', '2019-06-13 16:43:18', '', 'fotoPrevidenciario', '', 'inherit', 'open', 'closed', '', 'fotoprevidenciario', '', '', '2019-06-13 13:43:18', '2019-06-13 16:43:18', '', 0, 'http://neubertadvogados.hcdesenvolvimentos.com/wp-content/uploads/2019/06/fotoPrevidenciario.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2019-07-02 11:57:24', '2019-07-02 14:57:24', '<strong>Suporte em questões médicas e hospitalares.</strong>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consequat nisl vel pretium lectus quam id.</p>', 'Saúde', '', 'inherit', 'closed', 'closed', '', '84-autosave-v1', '', '', '2019-07-02 11:57:24', '2019-07-02 14:57:24', '', 84, 'http://neubertadvogados.hcdesenvolvimentos.com/2019/06/28/84-autosave-v1/', 0, 'revision', '', 0),
(95, 1, '2019-07-02 11:56:50', '2019-07-02 14:56:50', '<strong>Assessoria integral em demandas judiciais.</strong>\n<p>Vitae semper quis lectus nulla at volutpat diam ut venenatis. Tortor dignissim convallis aenean et tortor at risus viverra. Mauris pellentesque pulvinar pellentesque habitant morbi tristique.</p>', 'Cível', '', 'inherit', 'closed', 'closed', '', '35-autosave-v1', '', '', '2019-07-02 11:56:50', '2019-07-02 14:56:50', '', 35, 'http://neubertadvogados.hcdesenvolvimentos.com/2019/07/02/35-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_termmeta`
--

CREATE TABLE `nb_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_terms`
--

CREATE TABLE `nb_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_terms`
--

INSERT INTO `nb_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Menu principal', 'menu-principal', 0),
(3, 'Menu pages', 'menu-pages', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_term_relationships`
--

CREATE TABLE `nb_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_term_relationships`
--

INSERT INTO `nb_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(10, 2, 0),
(11, 2, 0),
(12, 2, 0),
(54, 2, 0),
(53, 2, 0),
(58, 2, 0),
(60, 3, 0),
(63, 3, 0),
(64, 3, 0),
(62, 3, 0),
(59, 3, 0),
(61, 3, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_term_taxonomy`
--

CREATE TABLE `nb_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_term_taxonomy`
--

INSERT INTO `nb_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6),
(3, 3, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_usermeta`
--

CREATE TABLE `nb_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_usermeta`
--

INSERT INTO `nb_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'neubertadvogados'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'nb_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'nb_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'nb_dashboard_quick_press_last_post_id', '96'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:13:\"187.255.131.0\";}'),
(19, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(20, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(21, 1, 'nb_r_tru_u_x', 'a:2:{s:2:\"id\";s:0:\"\";s:7:\"expires\";i:86400;}'),
(22, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:22:\"add-post-type-destaque\";i:1;s:21:\"add-post-type-servico\";i:2;s:12:\"add-post_tag\";i:3;s:21:\"add-categoriaDestaque\";}'),
(24, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:38:\"dashboard_right_now,dashboard_activity\";s:4:\"side\";s:62:\"redux_dashboard_widget,dashboard_quick_press,dashboard_primary\";s:7:\"column3\";s:0:\"\";s:7:\"column4\";s:0:\"\";}'),
(25, 1, 'nb_user-settings', 'libraryContent=browse&editor=html'),
(26, 1, 'nb_user-settings-time', '1559338988'),
(27, 1, 'nav_menu_recently_edited', '3');

-- --------------------------------------------------------

--
-- Estrutura para tabela `nb_users`
--

CREATE TABLE `nb_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `nb_users`
--

INSERT INTO `nb_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'neubertadvogados', '$P$Bh7uwHxp.LSCjKWKuF.xlycZ31vDsg/', 'neubertadvogados', 'devhcdesenvolvimentos@gmail.com', '', '2019-05-31 21:08:56', '', 0, 'neubertadvogados');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `nb_commentmeta`
--
ALTER TABLE `nb_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `nb_comments`
--
ALTER TABLE `nb_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Índices de tabela `nb_links`
--
ALTER TABLE `nb_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Índices de tabela `nb_options`
--
ALTER TABLE `nb_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Índices de tabela `nb_postmeta`
--
ALTER TABLE `nb_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `nb_posts`
--
ALTER TABLE `nb_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Índices de tabela `nb_termmeta`
--
ALTER TABLE `nb_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `nb_terms`
--
ALTER TABLE `nb_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Índices de tabela `nb_term_relationships`
--
ALTER TABLE `nb_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Índices de tabela `nb_term_taxonomy`
--
ALTER TABLE `nb_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Índices de tabela `nb_usermeta`
--
ALTER TABLE `nb_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `nb_users`
--
ALTER TABLE `nb_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `nb_commentmeta`
--
ALTER TABLE `nb_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `nb_comments`
--
ALTER TABLE `nb_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `nb_links`
--
ALTER TABLE `nb_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `nb_options`
--
ALTER TABLE `nb_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=919;

--
-- AUTO_INCREMENT de tabela `nb_postmeta`
--
ALTER TABLE `nb_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;

--
-- AUTO_INCREMENT de tabela `nb_posts`
--
ALTER TABLE `nb_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT de tabela `nb_termmeta`
--
ALTER TABLE `nb_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `nb_terms`
--
ALTER TABLE `nb_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `nb_term_taxonomy`
--
ALTER TABLE `nb_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `nb_usermeta`
--
ALTER TABLE `nb_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de tabela `nb_users`
--
ALTER TABLE `nb_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
